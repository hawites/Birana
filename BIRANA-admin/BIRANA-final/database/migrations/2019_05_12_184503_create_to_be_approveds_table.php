<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToBeApprovedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('to_be_approveds', function (Blueprint $table) {
             $table->increments('id');
        
          
          $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
              $table->unsignedInteger('book_id');

            $table->foreign('book_id')->references('book_id')->on('books')->onUpdate('cascade')->onDelete('cascade');


            $table->string('verification_image');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('to_be_approveds');
    }
}