<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
           $table->increments('book_id');
            $table->string('book_name');
           
              $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
       
            $table->string('genre');
            $table->double('price');
            $table->integer('average_rating')->default(0);
            $table->string('language')->nullable();
            $table->string('synopsis');
            $table->date('published_date');
            $table->integer('number_of_purchases')->default(0);
            $table->string('image');
            $table->string('file');
            $table->string('verification_image');
            $table->boolean('approved')->default(0);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));


        });
    } 

   /**
     * Run the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}