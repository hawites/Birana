<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/login/custom', [
	'uses' => 'LoginController@login',
	'as' => 'login.custom'
]);

Route::get('/booklist', 'BookListController@index');
Route::post('/addbook', 'BookListController@add');

Route::post('/editbook/{bid?}', 'BookListController@edit');
Route::post('/deletebook/{bid?}', 'BookListController@delete');

Route::post('/addcredit', 'CreditController@add');
Route::get('/editcreditform', function () {
    return view('editcredit');
});
Route::get('/addcreditform', function () {
    return view('add_credit');
});
Route::post('/editcredit/{cid?}', 'CreditController@edit');
Route::get('/deletecredit/{cid?}', 'CreditController@delete');
Route::get('/bookadmin', 'BookAdminController@index');
Route::get('/approve', 'BookAdminController@approve');
Route::get('/disapprove', 'BookAdminController@disapprove');

Route::get('/enc', function () {
    return view('encryption');
});