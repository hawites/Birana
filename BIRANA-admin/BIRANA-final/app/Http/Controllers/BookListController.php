<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class BookListController extends Controller
{
	  /**
     * Create a new controller instance.
     *
     * @return void
     */
	  public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	 if(Auth::user()->admin==1){
            return view('book-admin');
        }
        else {
            return view('book-list');
        }
    }

    public function add(Request $request)
    {
        $user_id= $_POST["id"];
    
      $name=$_POST["name"]; 
        $genre=$_POST["genre"]; 
        $author=$_POST["author"]; 
        $language=$_POST["language"]; 
        $synopsis=$_POST["synopsis"]; 
        $published=$_POST["published_date"]; 
        $price=$_POST["price"];
        $file = $request->file('file');
        $image=$request->file('image');
         $vimage=$request->file('vimage');

        
            $file_name=$file->getClientOriginalName();
            $image_name=$image->getClientOriginalName();
              $vimage_name=$vimage->getClientOriginalName();
               $files = array("pdf","epub"); 
        $images = array("jpg","png","jpeg"); 
        $extension_file = $file->getClientOriginalExtension();
         $extension_image = $image->getClientOriginalExtension();
        $extension_vimage = $vimage->getClientOriginalExtension();

            if (in_array($extension_file, $files)  && in_array($extension_image, $images) && in_array($extension_vimage, $images) )
            { 



                if($file->move('files', $file_name) &&  $image->move('images', $image_name) &&  $vimage->move('images', $vimage_name)){
                   

            
                    $inserted_id=DB::table('books')->insertGetId(array('book_name'=> $name,'genre'=>$genre,'user_id'=>$user_id,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price , 'file'=>$file_name, 'image'=>$image_name,'verification_image'=>$vimage_name));
          
                    return redirect('/booklist');
                }
                else{
                    return redirect('/booklist')->with('message', 'file upload failed. Please try adding again.');;
       
                }
     
            }
            else{
            return redirect('/booklist')->with('message', 'wrong file format.');
            }

        }


    
        

    
      public function edit(Request $request, $bid=null){

       

        $file = $request->file('file');
        $image=$request->file('image');
             $vimage=$request->file('vimage');
        $id=$_POST["id"];
         $name=$_POST["name"]; 
        $genre=$_POST["genre"]; 
       
        $language=$_POST["language"]; 
        $published=$_POST["published_date"]; 
        $synopsis=$_POST["synopsis"]; 
        $price=$_POST["price"]; 
    
   
        

        if($file==null && $image==null && $vimage==null)
        {
          DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'user_id'=>$id,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price ));
     
       return redirect('/booklist');
        } 
        elseif($file==null &&  $image!=null && $vimage==null){
                $image_name=$image->getClientOriginalName();
                 $extension_image = $image->getClientOriginalExtension();
                  $images = array("jpg","png","jpeg"); 
                if (in_array($extension_image, $images) )
                    { 
                    if($image->move('images', $image_name)){
                        
       DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre, 'user_id'=>$id,  'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price, 'image'=>$image_name ));
     
      return redirect('/booklist');
                    

                        
                }
                else{
                     return redirect('/booklist')->with('message', 'not uploading');
                }
            }
            else{
                 return redirect('/booklist')->with('message', 'wrong file format.');
            }



        }

          elseif($file==null &&  $image==null && $vimage!=null){
                $vimage_name=$vimage->getClientOriginalName();
                 $extension_vimage = $vimage->getClientOriginalExtension();
                  $images = array("jpg","png","jpeg"); 
                if (in_array($extension_vimage, $images) )
                    { 
                    if($vimage->move('images', $vimage_name)){
                        
       DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'user_id'=>$id,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price, 'verification_image'=>$vimage_name ));
     
      return redirect('/booklist');
                    

                        
                }
                else{
                     return redirect('/booklist')->with('message', 'not uploading');
                }
            }
            else{
                 return redirect('/booklist')->with('message', 'wrong file format.');
            }



        }
        elseif($file!=null &&  $image==null && $vimage==null){

            $file_name=$file->getClientOriginalName();
                 $extension_file = $file->getClientOriginalExtension();
                  $files= array("pdf","epub"); 
                if (in_array($extension_file, $files) )
                    { 
                    if($file->move('files', $file_name)){
                        
       DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'user_id'=>$id, 'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price, 'file'=>$file_name ));
     
      return redirect('/booklist');
                    

                        
                }
                else{
                     return redirect('/booklist')->with('message', 'not uploading');
                }
            }
            else{
                 return redirect('/booklist')->with('message', 'wrong file format.');
            }

        }
        else{
            $file_name=$file->getClientOriginalName();
            $image_name=$image->getClientOriginalName();
            $vimage_name=$vimage->getClientOriginalName();
               $files = array("pdf","epub"); 
        $images = array("jpg","png","jpeg"); 
        $extension_file = $file->getClientOriginalExtension();
         $extension_image = $image->getClientOriginalExtension();
             $extension_vimage = $vimage->getClientOriginalExtension();
          if (in_array($extension_file, $files)  && in_array($extension_image, $images) && in_array($extension_vimage, $vimages) )
          { 

            if($file->move('files', $file_name) &&  $image->move('images', $image_name) &&  $vimage->move('images', $vimage_name)){
    
   
    
         DB::table('books')->insert(array('book_name'=> $name,'genre'=>$genre,'user_id'=>$id,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price , 'file'=>$file_name, 'image'=>$image_name,'verification_image'=>$vimage_name));
      
          return redirect('/booklist');
      }
      else{
        return redirect('/booklist');
   
      }
     
            }
            else{
            return redirect('/booklist')->with('message', 'wrong file format.');
            }
        }

        
        
        
  
    }

       public function delete($bid=null){
          DB::table('books')
      ->where('book_id',$bid)
      ->delete();

         if(Auth::user()->admin==1){
            return view('bookadmin');
           
        }
        else {
       
              return redirect('/booklist');
        }
      

     
    }


}
