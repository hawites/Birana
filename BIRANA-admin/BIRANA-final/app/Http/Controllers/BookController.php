<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Book;


class BookController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('books');
    }


    public function add(Request $request)
    {
    
      $name=$_POST["name"]; 
        $genre=$_POST["genre"]; 
        $author=$_POST["author"]; 
        $language=$_POST["language"]; 
        $synopsis=$_POST["synopsis"]; 
        $published=$_POST["published_date"]; 
        $price=$_POST["price"];
    	$file = $request->file('file');
    	$image=$request->file('image');

    	
    		$file_name=$file->getClientOriginalName();
    		$image_name=$image->getClientOriginalName();
    		   $files = array("pdf","epub"); 
        $images = array("jpg","png","jpeg"); 
        $extension_file = $file->getClientOriginalExtension();
         $extension_image = $image->getClientOriginalExtension();
          if (in_array($extension_file, $files)  && in_array($extension_image, $images) )
		  { 

    		if($file->move('files', $file_name) &&  $image->move('images', $image_name)){
    	
    			


    



      
     
       
	
         $inserted_id=DB::table('books')->insertGetId(array('book_name'=> $name,'genre'=>$genre,'author'=>$author,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price , 'file'=>$file_name, 'image'=>$image_name));
      
          return redirect('/addcreditform?id='.$inserted_id);
      }
      else{
      	return redirect('/books');
   
      }
     
    		}
    		else{
    		return redirect('/books')->with('message', 'wrong file format.');
    		}

    	}
    
    	

    
     public function list(){
    	 return view('books');


    }

     public function edit(Request $request, $bid=null){

       

		$file = $request->file('file');
    	$image=$request->file('image');

    	 $name=$_POST["name"]; 
        $genre=$_POST["genre"]; 
        $author=$_POST["author"]; 
        $language=$_POST["language"]; 
        $published=$_POST["published_date"]; 
        $synopsis=$_POST["synopsis"]; 
        $price=$_POST["price"]; 
    
   
    	

    	if($file==null && $image==null)
    	{
          DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'author'=>$author,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price ));
     
       return redirect('/addcreditform');
    	} 
    	elseif($file==null &&  $image!=null){
    			$image_name=$image->getClientOriginalName();
    			 $extension_image = $image->getClientOriginalExtension();
    			  $images = array("jpg","png","jpeg"); 
    			if (in_array($extension_image, $images) )
		  			{ 
	    			if($image->move('images', $image_name)){
	    				
	   DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'author'=>$author,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price, 'image'=>$image_name ));
     
      return redirect('/addcreditform');
    				

		  				
    			}
    			else{
    				 return redirect('/books')->with('message', 'not uploading');
    			}
    		}
    		else{
    			 return redirect('/books')->with('message', 'wrong file format.');
    		}



    	}
    	elseif($file!=null &&  $image==null){

    		$file_name=$file->getClientOriginalName();
    			 $extension_file = $file->getClientOriginalExtension();
    			  $files= array("pdf","epub"); 
    			if (in_array($extension_file, $files) )
		  			{ 
	    			if($file->move('files', $file_name)){
	    				
	   DB::table('books') 
        -> where('book_id',$bid)
        ->update(array('book_name'=> $name,'genre'=>$genre,'author'=>$author,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price, 'file'=>$file_name ));
     
      return redirect('/addcreditform');
    				

		  				
    			}
    			else{
    				 return redirect('/books')->with('message', 'not uploading');
    			}
    		}
    		else{
    			 return redirect('/books')->with('message', 'wrong file format.');
    		}

    	}
    	else{
    		$file_name=$file->getClientOriginalName();
    		$image_name=$image->getClientOriginalName();
    		   $files = array("pdf","epub"); 
        $images = array("jpg","png","jpeg"); 
        $extension_file = $file->getClientOriginalExtension();
         $extension_image = $image->getClientOriginalExtension();
          if (in_array($extension_file, $files)  && in_array($extension_image, $images) )
		  { 

    		if($file->move('files', $file_name) &&  $image->move('images', $image_name)){
    
   
	
         DB::table('books')->insert(array('book_name'=> $name,'genre'=>$genre,'author'=>$author,   'language'=>$language,'synopsis'=>$synopsis,'published_date'=>$published,'price'=>$price , 'file'=>$file_name, 'image'=>$image_name));
      
          return redirect('/addcreditform');
      }
      else{
      	return redirect('/books');
   
      }
     
    		}
    		else{
    		return redirect('/books')->with('message', 'wrong file format.');
    		}
    	}

    	
    	
  
    }
    public function delete($bid=null){
          DB::table('books')
      ->where('book_id',$bid)
      ->delete();
      

       return redirect('/books');
    }

  

}
