<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class BookAdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
	  public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	 if(Auth::user()->admin==1){
            return view('book-admin');
        }
        else {
            return view('book-list');
        }
    }

    public function approve()
    {
    	   $bid=$_GET["book_id"]; 
    	   DB::table('books')
            ->where('book_id', $bid)
            ->update(['approved' => "1"]);

           return redirect('/bookadmin');

    }

     public function disapprove()
    {
    	   $bid=$_GET["book_id"]; 
    	   DB::table('books')
            ->where('book_id', $bid)
            ->update(['approved' => "2"]);

           return redirect('/bookadmin');

    }

    
}
