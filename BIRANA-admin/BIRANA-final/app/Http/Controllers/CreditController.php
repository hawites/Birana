<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Credit;


class CreditController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');

    }

    public function index($id=null)
    {

        return view('add_credit');
    }

    public function add()
    {
       $credits=Credit::all(); 
      $name=$_POST["name"]; 
        $percentage=$_POST["percentage"]; 
        $email=$_POST["email"]; 
        $phone="+251".$_POST["phone"]; 
        $book_id=$_POST["book_id"]; 

      $percent=$percentage;
      foreach($credits as $credit){
        if($credit->book_id==$book_id){
           $percent=$percent+$credit->percentage;
        }
      }
      if($percent>95){
           return redirect('/addcreditform?id='.$book_id)->with('message', 'try a lesser percentage');
      }
     
        
      
      
        
         DB::table('credits')->insert(array('name'=> $name,'percentage'=>$percentage,'email_address'=>$email,'account'=>$phone, 'book_id' => $book_id));
      
          return redirect('/addcreditform?id='.$book_id);



    }

     public function edit($cid=null){
  $credits=Credit::all(); 
        $name=$_POST["name"]; 
        $percentage=$_POST["percentage"]; 
        $email=$_POST["email"]; 
        $phone="+251".$_POST["phone"]; 
        $percent=$percentage;
      foreach($credits as $credit){
         if($credit->credit_id==$cid){
          $book_id=$credit->book_id;
      
      }
    }
      foreach($credits as $credit){
      if($credit->book_id==$book_id){
           $percent=$percent+$credit->percentage;
        }
      }

      
      if($percent>95){
        return redirect('/editcreditform?id='.$cid)->with('message', 'try a lesser percentage');
      }

        DB::table('credits') 
        -> where('credit_id',$cid)
        ->update(array('name'=> $name,'percentage'=>$percentage,'email_address'=>$email,'account'=>$phone));
     
       return redirect('/booklist');

    }

     public function delete($cid=null){
          DB::table('credits')
      ->where('credit_id',$cid)
      ->delete();
      

       return redirect('/booklist');
    }
}
