@extends('layouts.app')

@section('content')
<?php $book_id=$_GET['id']; ?>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Credit</div>

                <div class="card-body">
                    <form method="POST" action="/addcredit">
                        @csrf
                         <input id="book_id" type="hidden" class="form-control" name="book_id"  value="{{$book_id}}"required autofocus>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>

                            </div>
                        </div>

                        

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Account/Phone Number   (+251)</label>
                            
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone"  required autofocus>

                                @if ($errors->has('contact'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         


                        <div class="form-group row">
                            <label for="percentage" class="col-md-4 col-form-label text-md-right">Percentage</label>

                            <div class="col-md-6">
                                <input id="percentage" type="number" min="1" max="95" class="form-control" name="percentage" required autofocus>

                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-credit" style="background-color: #823521">
                                    Add Credit
                                </button>
                            </div>
                             <div class="col-md-2 ">
                                 <button type="button" class="btn btn-credit" onclick="location.href = '/booklist';" style="background-color: #823521">
                                   Back
                                </button>
                             </div>

                        </div>
                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection