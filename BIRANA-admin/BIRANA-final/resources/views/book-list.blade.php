@extends('layouts.app')

@section('content')
<?php
$books=App\Book::orderBy('created_at','desc')->get();  $i=1;
$credits=App\Credit::orderBy('created_at','desc')->get();
$fname=Auth::user()->first_name;
$lname=Auth::user()->last_name;
$userId = Auth::id();
$per="Not Full";
// $mytime = Carbon\Carbon::now();
// dd( $mytime);

$time=Carbon\Carbon::today()->toDateString();





?>

<html lang="en">
<head>
    <title>Books</title>
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/new.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="css/util1.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  

    
 </head>
 <body  style="max-width:100%;overflow-x:hidden;" color="#efe4b0">
   <div class="secondBody">
  
     <div class="container">
      <div class="row justify-content-center">
 
   <div class="col-md-4">
  <h2>List of Books</h2>
</div>

        <div class="col-md-4">
 <form class="form-inline md-form form-sm active-pink active-pink-2 mt-2">
  <i class="fa fa-search" aria-hidden="true"></i>
  <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
    aria-label="Search">
</form></div></div></div>

  @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
        
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
         <th>Name</th>
        <th>Genre</th>
        <th>Author</th>
        <th>Language</th>
        <th>Published Date</th>
        <th>Average Rating</th>
        <th>Number of Purchases</th>
          <th>File</th>
              <th>Book Cover</th>
              <th>Verification image</th>
        
        
      </tr>
    </thead>
    <tbody>
      
      @foreach ($books as $book)
      @if($book->user_id== Auth::id())



              
             
                  <?php $book_id=$book->book_id; ?>
               <tr>
                
                    <td > {{$i++}}</td>
                  
                    <td>{{ $book->book_name }}  </td>
                  
                      <td>{{ $book->genre }}</td>
                      <td>{{ $fname}}  {{$lname}} </td>
                      <td>{{ $book->language }}</td>
                     
                      <td>{{ $book->published_date }}</td>
                           <td>{{ $book->average_rating }}</td>
                      <td>{{ $book->number_of_purchases }}</td>
                    <td><a  href="/files/{{$book->file}}" ><b sytle="color:#823521;" >View</b></a></td>

          <td><img src="/images/{{$book->image}}" alt="book image" height="60" width="60"></td>
           <td><a  href="/files/{{$book->file}}" ><b sytle="color:#823521;" >View</b></a></td>

          <td><img src="/images/{{$book->verification_image}}" alt="book image" height="60" width="60"></td>
          <td>@if($book->approved==1)
            <b><p style="color:green;">Approved</p></b>

            @elseif($book->approved==2)

             <b> <p style="color:red;">Declined</p></b>

            @else
              <b> <p style="color:red;">Pending</p></b>

            @endif
          </td>
     
       
      
        <td>
    
         <a href="#" style="font-size:24px; color:green"  data-toggle="modal" data-target="#myModal{{$i}}"> <i class="fa fa-edit"></i></a>
        <a href="#" style="font-size:24px; color:red"  data-toggle="modal" data-target="#myDelete{{$i}}"> <i class="fa fa-trash"></i></a> 

        <div class="modal fade" id="myModal{{$i}}" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Book</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
        
@if($book->book_id==$book_id)

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            

                <div class="card-body">
                    <form method="POST" action="/editbook/{{$book_id}}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$book->book_name}}" required autofocus>

                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="genre" class="col-md-4 col-form-label text-md-right">Genre</label>

                            <div class="col-md-6">
                               
                                           
                                <select class="form-control" id="genre" name="genre" >
                                     
                                       
                                        <option <?php if($book->genre=="Non Fiction") echo "selected=\"selected\""; ?>  value="Non Fiction">Non Fiction</option>
                                        <option <?php if($book->genre=="Romance") echo "selected=\"selected\""; ?>  value="Romance">Romance</option>
                                        <option <?php if($book->genre=="Religious") echo "selected=\"selected\""; ?>  value="Religious">Religious</option>
                                        <option <?php if($book->genre=="Political") echo "selected=\"selected\""; ?>  value="Political">Political</option>
                                        <option <?php if($book->genre=="Fantasy") echo "selected=\"selected\""; ?>  value="Fantasy">Fantasy</option>
                                        <option <?php if($book->genre=="Children") echo "selected=\"selected\""; ?>  value="Children">Children</option>
                                         <option <?php if($book->genre=="Si-Fi") echo "selected=\"selected\""; ?>  value="Si-Fi">Si-Fi</option>
                                          <option <?php if($book->genre=="Horror/Thriller") echo "selected=\"selected\""; ?> value="Horror/Thriller">Horror/Thriller</option>
                              


                                </select>
                                        </div>
                        </div>

                       
   <input id="id" type="hidden" class="form-control" name="id" value="{{$userId}}" required autofocus>
                              
                       
                          <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">Price</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" value="{{$book->price}}" required autofocus>

                            </div>
                        </div>

                      <div class="form-group row">
                            <label for="language" class="col-md-4 col-form-label text-md-right">Language</label>

                            <div class="col-md-6">
                                           
                                <select class="form-control" id="language" name="language"  >
                                   
                                         <option <?php if($book->language=="Amharic") echo "selected=\"selected\""; ?>  value="Amharic">Amharic</option>
                                         <option <?php if($book->language=="Afaan Oromo") echo "selected=\"selected\""; ?>  value="Afaan Oromo">Afaan Oromo</option>
                                         <option <?php if($book->language=="Tigrigna") echo "selected=\"selected\""; ?>  value="Tigrigna">Tigrigna</option>

                                </select>
                                        </div>
                        </div>

                         <div class="form-group row">
                            <label for="synopsis" class="col-md-4 col-form-label text-md-right">Synopsis</label>

                            <div class="col-md-6">
                                <textarea id="synopsis" class="form-control" name="synopsis"   required autofocus=""> {{$book->synopsis}}</textarea>
                                

                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="published_date" class="col-md-4 col-form-label text-md-right">Published Date</label>

                            <div class="col-md-6">
<input value="{{$book->published_date}}"  type="date" id="published_date" class="form-control" name="published_date" min="1600-01-01" max={{$time}} required>
                                

                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">Upload New Book</label>

                            <div class="col-md-6">
                                <input  type="file" id="file"  name="file" >
                                

                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">Upload New Image</label>

                            <div class="col-md-6">
                                <input  type="file" id="image"  name="image" >
                                

                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="vimage" class="col-md-4 col-form-label text-md-right">Upload New Verification Image</label>

                            <div class="col-md-6">
                                <input  type="file" id="vimage"  name="vimage" >
                                

                            </div>
                        </div>
                        <?php $percent=0;?>
                        
                        @if($book->approved==1)

                          <div class="form-group row">
                               <label for="published_date" class="col-md-4 col-form-label text-md-right">Credits</label>

 <div class="col-md-6">
  @foreach($credits as $credit)

  @if($credit->book_id==$book->book_id)
  
 <a href="editcreditform?id={{$credit->credit_id}}" sytle="color:black;">{{$credit->name}}</a>  
 <a href= "/deletecredit/{{$credit->credit_id}}" class="delete"   title="Delete"><span class="fa fa-trash fa-2x text-danger "></span></a><br>

  @endif
  @endforeach

   <button type="button" class="btn btn-credit" onclick="location.href = '/addcreditform?id={{$book->book_id}}';">
                                   Add Credit
            </button>
 </div>







                          </div>
                          @endif




                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" style="background-color: #823521; border-color:#823521;">
                                    Edit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



        </div>  
        
      
      </div>
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="myDelete{{$i}}" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Delete Book</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        
        Are you sure to sure you want to delete this book?
        
        </div>
        <div class="modal-footer">
 <form method="POST" action="/deletebook/{{$book->book_id}}">
                        @csrf
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>


      </td>


    

      </tr>
      @endif
       @else
         <p>No books added.</p>
      @endif
     

    
      @endforeach

    </tbody>
  </table>
</div>
  <a style="font-size:26px;" data-toggle="modal" data-target="#myAdd"><i class="fa fa-plus-circle add"></i></a>
<div class="modal fade" id="myAdd" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Book</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
   <?php $i=1;
   $userId = Auth::id();
              ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              
                <div class="card-body">
                    <form method="POST" action="/addbook" enctype="multipart/form-data">
                        @csrf
   <input id="id" type="hidden" class="form-control" name="id" value="{{$userId}}" required autofocus>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>

                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="genre" class="col-md-4 col-form-label text-md-right">Genre</label>

                            <div class="col-md-6">
                                           
                              

   


<input list="browsers" name="genre" id="genre" /></label>
<datalist id="browsers">
   <option value="" disabled selected>Select Genre</option>
                                        <option value="Horror/Thriller">Horror/Thriller</option>
                                        <option value="Non Fiction">Non Fiction</option>
                                        <option value="Fiction">Fiction</option>
                                        <option value="Historical Fiction">Historical Fiction</option>
                                        <option value="Non Fiction">Non Fiction</option>
                                        <option value="Romance">Romance</option>
                                        <option value="Religious">Religious</option>
                                        <option value="Political">Political</option>
                                        <option value="Fantasy">Fantasy</option>
                                        <option value="Children">Children</option>
                                         <option value="Si-Fi">Si-Fi</option>
</datalist>

               
                                        </div>
                        </div>

                         <div class="form-group row">
                            <label for="author" class="col-md-4 col-form-label text-md-right">Author</label>

                            <div class="col-md-6">
                              
 <input id="author" type="text" class="form-control" name="author" required autofocus>
                             

                            </div>
                        </div>
                          <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">Price</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" required autofocus>

                            </div>
                        </div>

                      <div class="form-group row">
                            <label for="language" class="col-md-4 col-form-label text-md-right">Language</label>

                            <div class="col-md-6">
                                           
                                <select class="form-control" id="language" name="language"  required>
                                    <option value="" disabled selected>Select Language</option>
                                         <option value="Amharic">Amharic</option>
                                         <option value="Afaan Oromo">Afaan Oromo</option>
                                         <option value="Tigrigna">Tigrigna</option>

                                </select>
                                        </div>
                        </div>

                         <div class="form-group row">
                            <label for="synopsis" class="col-md-4 col-form-label text-md-right">Synopsis</label>

                            <div class="col-md-6">
                                <textarea id="synopsis" class="form-control" name="synopsis" required autofocus=""></textarea>
                                

                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="published_date" class="col-md-4 col-form-label text-md-right">Published Date</label>

                            <div class="col-md-6">
                                <input  type="date" id="published_date" class="form-control" name="published_date" required>
                                

                            </div>
                        </div>

                           <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">Upload  Book</label>

                            <div class="col-md-6">
                                <input  type="file" id="file"  name="file" >
                                

                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">Upload Image</label>

                            <div class="col-md-6">
                                <input  type="file" id="image"  name="image" >
                                

                            </div>
                        </div>

                        
                         <div class="form-group row">
                            <label for="vimage" class="col-md-4 col-form-label text-md-right">Verification Image</label>

                            <div class="col-md-6">
                                <input  type="file" id="vimage"  name="vimage" >
                                

                            </div>
                        </div>
       


                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success" style="background-color: #823521; border-color:#823521;">
                                    Add
                                </button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  


      </div>
    </div>
  </div>
    </div>
</div>
     </div> 
 </body>
 </html>
@endsection