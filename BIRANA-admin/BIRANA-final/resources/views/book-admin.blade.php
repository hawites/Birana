@extends('layouts.app')

@section('content')
<?php
$books=App\Book::orderBy('created_at','desc')->get();;  $i=1;
$credits=App\Credit::orderBy('created_at','desc')->get(); 
$fname=Auth::user()->first_name;
$lname=Auth::user()->last_name;
  $userId = Auth::id();


?>
<html lang="en">
<head>
    <title>Books</title>
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/new.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="css/util1.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  

    
 </head>
 <body  style="max-width:100%;overflow-x:hidden;" color="#efe4b0">
   <div class="secondBody">
  
     <div class="container">
  <h2>List of Books</h2>
  @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
        
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
         <th>Name</th>
        <th>Genre</th>
        <th>Author</th>
        <th>Language</th>
        <th>Published Date</th>
          <th>File</th>
          <th>Verification image</th>
           <th>Status</th>
        
      </tr>
    </thead>
    <tbody>
      
      @foreach ($books as $book)
  



              
             
                  <?php $book_id=$book->book_id; ?>
               <tr>
                
                    <td > {{$i++}}</td>
                  
                    <td>{{ $book->book_name }}  </td>
                  
                      <td>{{ $book->genre }}</td>
                      <td>{{ $fname}}  {{$lname}} </td>
                      <td>{{ $book->language }}</td>
                     
                      <td>{{ $book->published_date }}</td>
                    <td><a  href="/files/{{$book->file}}" ><b sytle="color:#823521;" >View</b></a></td>

       
       

          <td><a href="/images/{{$book->verification_image}}"><img src="/images/{{$book->verification_image}}" alt="book image" height="60" width="60"></a></td>
          <td>@if($book->approved==1)
            <b><p style="color:green;">Approved</p></b>
            @elseif($book->approved==2)
              <b><p style="color:green;">Declined</p></b>
            @else
            <a href="/approve?book_id={{$book->book_id}}" style="font-size:24px; color:green"> <i class="fa fa-check"></i></a> / <a href="/disapprove?book_id={{$book->book_id}}" style="font-size:24px; color:red"> <i class="fa fa-times"></i></a> 
            @endif
          </td>
     
       
      
        <td>
    
   
        <a href="#" style="font-size:24px; color:red"  data-toggle="modal" data-target="#myDelete{{$i}}"> <i class="fa fa-trash"></i></a> 

  
  <!-- Modal -->
  <div class="modal fade" id="myDelete{{$i}}" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Delete Book</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        
        Are you sure to sure you want to delete this book?
        
        </div>
        <div class="modal-footer">
 <form method="POST" action="/deletebook/{{$book->book_id}}">
                        @csrf
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>


      </td>


    

      </tr>
     

    
      @endforeach

    </tbody>
  </table>
</div>


</div>
     </div> 
 </body>
 </html>
@endsection