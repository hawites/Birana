@extends('layouts.app')

@section('content')
<?php $credit_id = $_GET['id'];   $credits=App\Credit::all();

?>

@foreach($credits as $credit)
@if($credit->credit_id==$credit_id)
<?php $phone =$credit->account;
$pos = 4;
$number = substr($phone, $pos);  ?>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Credit</div>

                <div class="card-body">
                    <form method="POST" action="/editcredit/{{$credit_id}}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"  value="{{$credit->name}}" required autofocus>

                            </div>
                        </div>

                       


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$credit->email_address}}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone Number   (+251)</label>
                            
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone"  value="{{$number}}" required autofocus>

                                @if ($errors->has('contact'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span >                               @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="percentage" class="col-md-4 col-form-label text-md-right">Percentage</label>

                            <div class="col-md-6">
                                <input id="percentage" type="number" min="1" max="100" class="form-control" name="percentage"  value="{{$credit->percentage}}" required autofocus>

                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-credit" style="background-color: #823521; border-color:#823521;">
                                    Edit
                                </button>
                            </div>
                              <div class="col-md-2 ">
                                 <button type="button" class="btn btn-credit" onclick="location.href = '/booklist';" style="background-color: #823521">
                                   Back
                                </button>
                             </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endforeach
@endsection