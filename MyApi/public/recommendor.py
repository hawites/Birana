#!/usr/bin/env
import pandas as pd
import numpy as np
from sklearn.neighbors import *
import warnings
import mysql.connector
import mysql.connector
from mysql.connector import Error
import csv
import io
warnings.filterwarnings('ignore')


# book_t = pd.read_csv('books.csv', sep=',')
# book_g = pd.read_csv('bookgenres.csv', sep=',')
# book_t = book_t.drop(['approved', 'image', 'file', 'updated_at'], axis=1)
# book_t.published_date.unique()
# book_t.loc[book_t.published_date.isnull(), :]
# purchased = book_t[book_t.number_of_purchases != 0]
# bestsellers = purchased.sort_values(
#     'number_of_purchases', ascending=False).head(10)
# check for NaN values
# rate_review = pd.read_csv('/Users/mac/Downloads/result/rating_reviews.csv', sep=',')
# rate_review.loc[rate_review.review.isnull(),:]
# rate_review.loc[(rate_review.id==2),'review']=''
# userdata = pd.read_csv('/Users/mac/Downloads/result/users.csv', sep=',')
# shelfdata = pd.read_csv('/Users/mac/Downloads/result/shelves.csv', sep=',')
# raterev=rate_review[rate_review.book_id.isin(bookdata.id)]  #keep rated books are inside the bookdata
# raterev=raterev[raterev.user_id.isin(userdata.id)]  #keep user rates that are only have been rated with users that exsist in the userdata



def suggestbygenre(genre_ids, lang):
    book_t = pd.read_csv('books.csv', sep=',')
    book_g = pd.read_csv('bookgenres.csv', sep=',')

    book_t.published_date.unique()
    book_t.loc[book_t.published_date.isnull(), :]
    book_t = book_t[['id', 'average_rating', 'language_id']]
    blist = pd.DataFrame()

    for i in range(len(genre_ids)):
        gen = book_g[book_g.genre_code == genre_ids[i]]
        bookbg = pd.merge(book_t, gen, left_on='id', right_on='book_id').sort_values(
            'average_rating', ascending=False).head(8)
        bookbg = bookbg[bookbg.language_id == lang]
        blist = blist.append(bookbg)

    bestb = pd.DataFrame()
    blist = blist.reset_index(drop=True)
    bestofgenre = blist.sort_values('average_rating', ascending=False)
    dupi = bestofgenre[bestofgenre.duplicated(['id'])]
    dupi = dupi.reset_index(drop=True)
    bestb = bestb.append(bestofgenre[bestofgenre.duplicated(['id'])])
    bestofgenre = bestofgenre[bestofgenre['id'].values != dupi['id'].values]

    beste = bestofgenre.head(8-len(bestb))

    var = 0
    i = 0
    while var == 0:
        count = beste['genre_code'].value_counts().values.tolist()
        indexb = beste['genre_code'].value_counts().index.tolist()
        if count[i] > 4:
            bge = beste.sort_values('average_rating', ascending=True)
            bge = bge[bge.genre_code == indexb[i]].head(count[i] - 2)

            bestofgenre = bestofgenre[bestofgenre['id'].values !=
                                      bge['id'].values]

            beste = bestofgenre.head(8 - len(bestb))
        else:
            var = 1
        i += 1

    bestb = bestb.append(beste)
    bestb = bestb.sort_values('average_rating', ascending=False)
    bestb = bestb[['id']]
    bestb = list(bestb.values.flatten())

    return bestb

    # print(besta)

    # print(bookbygenre.sort_values('average_rating',ascending=False).head(10))


# ratings_count=pd.DataFrame(raterev.groupby(['book_id'])['rating'].sum())
# popularbooks=ratings_count.sort_values('rating',ascending=False).head()

# ratings_matrix=raterev.pivot(index='user_id', columns='book_id', values='rating')

# def findksimilarusers(user_id, ratings ,k=0):
# 	similarities=[]
# 	indices=[]
# 	matric=sorted(VALID_METRICS['brute'])
# 	model_knn=NearestNeighbors(metric=matric[1],algorithm='brute')
# 	model_knn.fit(ratings)
# 	loc=ratings.index.get_loc(user_id)
# 	distances, indices = model_knn.kneighbors(ratings.iloc[loc,:].values.reshape(1,-1),n_neighbors=k+1)
# 	similarities=1-distances.flatten()
# 	return similarities,indices
# sim=findksimilarusers(1,ratings_matrix)[0]


# def suggestbyauthor(book_id,bookdata,book_t):
# 	auth=bookdata[bookdata.id==book_id].author_name
# 	bookbyauthor=bookdata[bookdata.author_name==auth[0]]
# 	bookbyauthor=pd.merge(book_t, bookbyauthor, left_on='id', right_on='book_id')
# 	bookbyauthor=bookbyauthor.sort_values('average_rating',ascending=False).head(10)
# 	bookbyauthor=bookbyauthor[bookbyauthor.id_x!=book_id]

# genre_ids = [6,12,11,2]
# suggestbygenre(genre_ids,book_g,book_t,2)
# suggestbygenre(1,bookdata,book_t)
