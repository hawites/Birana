<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../includes/DbOperations.php';
$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$app->post('/createuser', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('name', 'email', 'password'), $request, $response)) {

        $request_data = $request->getParsedBody();

        $name = $request_data['name'];
        $email = $request_data['email'];
        $password = $request_data['password'];

        $hash_password = password_hash($password, PASSWORD_DEFAULT);
        $db = new DbOperations;

        $result = $db->createUser($name, $email, $hash_password);


        if ($result == USER_CREATED) {
            $message = array();
            $message['error'] = false;
            $message['message'] = 'User created successfully';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else if ($result == USER_FAILURE) {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else if ($result == USER_EXISTS) {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'User already exists';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});

$app->post('/userlogin', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('email', 'password'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $email = $request_data['email'];
        $password = $request_data['password'];
        $db = new DbOperations;
        $result = $db->userLogin($email, $password);
        if ($result == USER_AUTHENTICATED) {
            $user = $db->getUserByEmail($email);


            $response_data['error'] = false;
            $response_data['message'] = 'Login Successful';
            $response_data['user'] = $user;
            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else if ($result == USER_NOT_FOUND) {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "User doesn't exist";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else if ($result == USER_PASSWORD_DO_NOT_MATCH) {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Incorrect email or password";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updateuser/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    if (!haveEmptyParameters(array('email', 'name'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $email = $request_data['email'];
        $name = $request_data['name'];

        $db = new DbOperations;
        if ($db->updateUser($id, $name, $email)) {
            $response_data = array();
            $response_data['error'] = false;
            $response_data['message'] = 'User profile updated successfully';

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Error! Try again later";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updatepassword', function (Request $request, Response $response) {

    if (!haveEmptyParameters(array('currentpass', 'newpass', 'email'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $email = $request_data['email'];
        $currentp = $request_data['currentpass'];
        $newp = $request_data['newpass'];

        $db = new DbOperations;
        $result = $db->updatepassword($currentp, $newp, $email);
        if ($result == PASSWORD_CHANGED) {
            $response_data = array();
            $response_data['error'] = false;
            $response_data['message'] = 'User password updated successfully';

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else if ($result == PASSWORD_DO_NOT_MATCH) {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Passwords do not match";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        } else if ($result == PASSWORD_NOT_CHANGED) {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Error! Try again later";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updateshelfcount/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    if (!haveEmptyParameters(array('state'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $state = $request_data['state'];


        $db = new DbOperations;
        if ($db->updateShelfCount($id, $state)) {
            $response_data = array();
            $response_data['error'] = false;
            $response_data['message'] = 'User shelf count updated successfully';

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Error! Try again later";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->delete('/deleteuser/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $db = new DbOperations;
    if ($db->deleteUser($id)) {
        $response_data = array();
        $response_data['error'] = false;
        $response_data['message'] = 'User deleted successfully';

        $response->write(json_encode($response_data));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    } else {
        $response_data = array();
        $response_data['error'] = true;
        $response_data['message'] = "Error! Try again later";

        $response->write(json_encode($response_data));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(422);
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->get('/allbooks', function (Request $request, Response $response) {
    $db = new DbOperations;
    $books = $db->getAllBooks();
    $response_data = array();

    $response_data['error'] = false;
    $response_data['books'] = $books;

    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/getbookbytitle', function (Request $request, Response $response) {

    $request_data = $request->getQueryParams();
    $title = $request_data['title'];



    $db = new DbOperations;
    $db->exportToCsv();
    $book = $db->getBookByTitle($title);
    $response_data = array();

    $response_data['error'] = false;
    $response_data['book'] = $book;
    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/search', function (Request $request, Response $response) {

    $request_data = $request->getQueryParams();
    $key = $request_data['key'];



    $db = new DbOperations;

    $book = $db->search($key);
    $response_data = array();
    // var_dump($book);
    // die();
    $response_data['error'] = false;
    $response_data['book'] = $book;
    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});
$app->get('/getgenre', function (Request $request, Response $response) {
    // if (!haveEmptyParameters(array(''), $request, $response)) {
    $request_data = $request->getQueryParams();

    $language = $request_data['lang'];

    $db = new DbOperations;
    $genre = $db->getAllGenres($language);
    $response_data = array();

    $response_data['error'] = false;
    $response_data['genre'] = $genre;
    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);

    // return $response
    //     ->withHeader('Content-Type', 'application/json')
    //     ->withStatus(422);
});
$app->get('/getshelf', function (Request $request, Response $response) {
    // if (!haveEmptyParameters(array(''), $request, $response)) {
    $request_data = $request->getQueryParams();
    $uid = $request_data['userid'];

    $st = $request_data['state'];
    $language = $request_data['lang'];

    $db = new DbOperations;
    $shelf = $db->getShelfbyState($uid, $st, $language);
    $response_data = array();

    $response_data['error'] = false;
    $response_data['shelf'] = $shelf;
    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);

    // return $response
    //     ->withHeader('Content-Type', 'application/json')
    //     ->withStatus(422);
});
$app->get('/getcustomshelf', function (Request $request, Response $response) {

    $request_data = $request->getQueryParams();
    $uid = $request_data['userid'];

    $language = $request_data['lang'];

    $db = new DbOperations;
    $shelf = $db->getShelf($uid, $language);

    $response_data = array();

    $response_data['error'] = false;
    $response_data['shelf'] = $shelf;
    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);

    // return $response
    //     ->withHeader('Content-Type', 'application/json')
    //     ->withStatus(422);
});
$app->post('/addtoshelf', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('userid', 'title', 'state'), $request, $response)) {

        $request_data = $request->getParsedBody();

        $userid = $request_data['userid'];
        $title = $request_data['title'];
        $state = $request_data['state'];



        $percent = 0;
        $startd = null;
        $finishd = null;

        if (isset($request_data['percent']) & $request_data['percent'] != "") {
            $percent = $request_data['percent'];
        }
        if (isset($request_data['start_date']) & $request_data['start_date'] != "") {
            $startd = $request_data['start_date'];
        }
        if (isset($request_data['finish_date']) & $request_data['finish_date'] != "") {
            $finishd = $request_data['finish_date'];
        }
        $db = new DbOperations;

        $result = $db->addToShelf($userid, $title, $state, $startd, $finishd, $percent);


        if ($result == BOOK_ADDED_TO_SHELF) {
            $message = array();
            $user = $db->updateShelfCount($userid, $state);
            $message['error'] = false;
            $message['message'] = 'Book added to shelf successfully';
            $message['user'] = $user;
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else if ($result == ADD_FAILURE) {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else if ($result == BOOK_EXISTS_IN_SHELF) {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Book exists in shelf';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updateshelf', function (Request $request, Response $response) {

    if (!haveEmptyParameters(array('userid', 'title', 'state'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $state = $request_data['state'];
        $title = $request_data['title'];
        $id = $request_data['userid'];
        $percent = 0;
        $startd = null;
        $finishd = null;

        if (isset($request_data['percent']) & $request_data['percent'] != "") {
            $percent = $request_data['percent'];
        }
        if (isset($request_data['start_date']) & $request_data['start_date'] != "") {
            $startd = $request_data['start_date'];
        }
        if (isset($request_data['finish_date']) & $request_data['finish_date'] != "") {
            $finishd = $request_data['finish_date'];
        }


        $db = new DbOperations;
        if ($db->updateShelf($id, $title, $state, $startd, $finishd, $percent)) {
            $response_data = array();
            $response_data['error'] = false;
            $response_data['message'] = 'Shelf updated successfully';

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Error! Try again later";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->delete('/deletebookfromshelf', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('userid', 'title'), $request, $response)) {
        $request_data = $request->getParsedBody();


        $title = $request_data['title'];
        $id = $request_data['userid'];
        $db = new DbOperations;
        if ($db->deleteBookFromShelf($title, $id)) {
            $response_data = array();
            $response_data['error'] = false;
            $response_data['message'] = 'Book deleted from shelf successfully';

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        } else {
            $response_data = array();
            $response_data['error'] = true;
            $response_data['message'] = "Error! Try again later";

            $response->write(json_encode($response_data));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(422);
    }
});
$app->post('/addratereview', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('userid', 'title', 'rating', 'has_spoiler'), $request, $response)) {

        $request_data = $request->getParsedBody();

        $uid = $request_data['userid'];
        $title = $request_data['title'];
        $rating = $request_data['rating'];
        $has_spoiler = $request_data['has_spoiler'];
        $review = null;
        if (isset($request_data['review']) & $request_data['review'] != "") {
            $review = $request_data['review'];
        }

        $db = new DbOperations;
        $result = $db->addRateReview($uid, $title, $rating, $review, $has_spoiler);

        if ($result) {
            $message = array();
            $message['error'] = false;
            $message['message'] = 'Rate/review added successfully';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updateratereview', function (Request $request, Response $response) {

    if (!haveEmptyParameters(array('userid', 'title', 'rating', 'has_spoiler'), $request, $response)) {
        $request_data = $request->getParsedBody();

        $uid = $request_data['userid'];
        $title = $request_data['title'];
        $rating = $request_data['rating'];
        $has_spoiler = $request_data['has_spoiler'];
        $review = null;
        if (isset($request_data['review']) & $request_data['review'] != "") {
            $review = $request_data['review'];
        }

        $db = new DbOperations;
        $result = $db->updateRateReview($uid, $title, $rating, $review, $has_spoiler);

        if ($result) {
            $message = array();
            $message['error'] = false;
            $message['message'] = 'Rate/review updated successfully';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->post('/addcomment', function (Request $request, Response $response) {
    if (!haveEmptyParameters(array('userid', 'title', 'comment', 'has_spoiler'), $request, $response)) {

        $request_data = $request->getParsedBody();

        $uid = $request_data['userid'];
        $title = $request_data['title'];
        $comment = $request_data['comment'];
        $has_spoiler = $request_data['has_spoiler'];


        $db = new DbOperations;
        $result = $db->addComment($uid, $title, $comment, $has_spoiler);

        if ($result) {
            $message = array();
            $message['error'] = false;
            $message['message'] = 'Comment added successfully';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});
$app->put('/updatecomment', function (Request $request, Response $response) {

    if (!haveEmptyParameters(array('userid', 'title', 'comment', 'has_spoiler'), $request, $response)) {
        $request_data = $request->getParsedBody();

        $uid = $request_data['userid'];
        $title = $request_data['title'];
        $comment = $request_data['comment'];
        $has_spoiler = $request_data['has_spoiler'];


        $db = new DbOperations;
        $result = $db->updateComent($uid, $title, $comment, $has_spoiler);

        if ($result) {
            $message = array();
            $message['error'] = false;
            $message['message'] = 'Comment updated successfully';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        } else {
            $message = array();
            $message['error'] = true;
            $message['message'] = 'Error';
            $response->write(json_encode($message));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(422);
        }
    }
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(422);
});


//rec
$app->get('/getExplorableBooks', function (Request $request, Response $response) {
    $request_data = $request->getQueryParams();
    $genres = array();
    $genres = $request_data['genres'];
    $lang = $request_data['lang'];


    $db = new DbOperations;
    $gbooks = $db->getGenreSimilarBooks($genres, $lang);
    $pbooks = $db->popular();

    $bbooks = $db->bestSellers($lang);
    $response_data = array();

    $response_data['error'] = false;
    $response_data['gbooks'] = $gbooks;
    $response_data['pbooks'] = $pbooks;
    $response_data['bbooks'] = $bbooks;

    $response->write(json_encode($response_data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus(200);
});





function haveEmptyParameters($required_params, $request, $response)
{
    $error = false;
    $error_params = '';
    $request_params = $request->getParsedBody();

    foreach ($required_params as $param) {

        if (!isset($request_params[$param]) || strlen($request_params[$param]) <= 0) {

            $error = true;
            $error_params .= $param . ', ';
        }
    }
    if ($error) {
        $error_detail = array();
        $error_detail['error'] = true;
        $error_detail['message'] = 'Required parameters ' . substr($error_params, 0, -2) . ' are missing or empty';
        $response->write(json_encode($error_detail));
    }
    return $error;
}

$app->run();
