<?php

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "birana");

define("USER_CREATED", 101);
define("USER_EXISTS", 102);
define("USER_FAILURE", 103);

define("USER_AUTHENTICATED", 201);
define("USER_NOT_FOUND", 202);
define("USER_PASSWORD_DO_NOT_MATCH", 203);

define("BOOK_ADDED_TO_SHELF", 301);
define("BOOK_EXISTS_IN_SHELF", 302);
define("ADD_FAILURE", 303);

define("PASSWORD_CHANGED", 401);
define("PASSWORD_DO_NOT_MATCH", 402);
define("PASSWORD_NOT_CHANGED", 403);
