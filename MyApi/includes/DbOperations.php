<?php

class DbOperations
{
    private $con;

    //establish Db connection
    function __construct()
    {
        include_once dirname(__FILE__) . '/DbConnect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }
    //create user
    public function createUser($name, $email, $password)
    {

        if (!$this->isEmailExist($email)) {

            $stmt = $this->con->prepare("insert into users(name, email, password) values(?,?,?)");

            $stmt->bind_param("sss", $name, $email, $password);

            if ($stmt->execute()) {
                return USER_CREATED;
            } else {
                return USER_FAILURE;
            }
        }
        return USER_EXISTS;
    }

    //user login
    public function userLogin($email, $password)
    {
        if ($this->isEmailExist($email)) {
            $r = $this->getPasswordByEmail($email);

            if (password_verify($password, $r['password'])) {
                $val = 2;
                if ($r['logstat'] == 0) {
                    $val = 1;
                } else if ($r['logstat'] == 1) {
                    $val = 2;
                }
                $stmt = $this->con->prepare("update users set never_logged_in = ? where email=?");
                $stmt->bind_param("is",  $val, $email);

                $stmt->execute();
                return USER_AUTHENTICATED;
            } else {
                return USER_PASSWORD_DO_NOT_MATCH;
            }
        } else {
            return USER_NOT_FOUND;
        }
    }

    //update user info
    public function updateUser($userid, $name, $email)
    {
        $stmt = $this->con->prepare("update users set name=?, email=? where id=?");

        $stmt->bind_param("ssi", $name, $email, $userid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function updatePrefferedLang($lang, $userid)
    {
        $stmt = $this->con->prepare("update users set prefferedLang=? where user_id=?");

        $stmt->bind_param("si", $lang, $userid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    //update user password
    public function updatePassword($currentpass, $newpass, $email)
    {
        $r = $this->getPasswordByEmail($email);

        if (password_verify($currentpass, $r['password'])) {
            $hash = password_hash($newpass, PASSWORD_DEFAULT);

            $stmt = $this->con->prepare("update users set password=? where email=?");
            $stmt->bind_param("ss", $hash, $email);
            if ($stmt->execute()) {
                return PASSWORD_CHANGED;
            }
            return PASSWORD_NOT_CHANGED;
        } else {
            return PASSWORD_DO_NOT_MATCH;
        }
    }

    //delete user
    public function deleteUser($id)
    {
        $stmt = $this->con->prepare('delete from users where id =?');
        $stmt->bind_param('i', $id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function getUserByEmail($email)
    {
        $stmt = $this->con->prepare("call getUserByEmail(?)");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->bind_result($id, $name, $email, $crc, $wrc, $rc, $nl, $prefl);
        $stmt->fetch();
        $user = array();
        $user['id'] = $id;
        $user['email'] = $email;
        $user['name'] = $name;
        $user['crcount'] = $crc;
        $user['wrcount'] = $wrc;
        $user['rcount'] = $rc;
        $user['log_status'] = $nl;
        $user['prefered_lang'] = $prefl;

        return $user;
    }
    private function getPasswordByEmail($email)
    {
        $stmt = $this->con->prepare("select password, never_logged_in from users where email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->bind_result($password, $logstatus);
        $stmt->fetch();
        $result = array();
        $result['password'] = $password;
        $result['logstat'] = $logstatus;
        return $result;
    }

    public function isEmailExist($email)
    {
        $stmt = $this->con->prepare("select id from users where email=? and role=0");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows > 0;
    }
    //update shelf count
    public function updateShelfCount($userid, $state)
    {
        $message = 'Rcount';
        switch ($state) {
            case 'CR':
                $message = 'CRcount';
                break;
            case 'WR':
                $message = 'WtRcount';
                break;
            default:
                $message = 'Rcount';
        }
        $stmt = $this->con->prepare("update users set " . $message . " = " . $message . " + 1 where id=?");
        $stmt->bind_param('i', $userid);
        $user = array();
        if ($stmt->execute()) {
            $stmt2 = $this->con->prepare("call getUserById(?)");
            $stmt2->bind_param("s", $userid);
            $stmt2->execute();
            $stmt2->bind_result($name, $email, $crc, $wrc, $rc, $nl, $prefl);
            $stmt2->fetch();

            $user['id'] = $userid;
            $user['email'] = $email;
            $user['name'] = $name;
            $user['crcount'] = $crc;
            $user['wrcount'] = $wrc;
            $user['rcount'] = $rc;
            $user['log_status'] = $nl;
            $user['prefered_lang'] = $prefl;
        }
        return $user;
    }

    public function addToShelf($userid, $title, $state, $start_date, $finish_date, $percent)
    {
        if (!$this->isBookExistInShelf($title, $userid)) {

            $id = $this->getBookIdByTitle($title);

            $type = 0;
            if ($state != "CR" & $state != "WR" & $state != "R") {
                $type = 1;
            }

            $stmt = $this->con->prepare("call addToShelf(?,?,?,?,?,?,?)");
            $stmt->bind_param("iisidss", $id, $userid, $state, $type, $percent, $start_date, $finish_date);



            if ($stmt->execute()) {
                return BOOK_ADDED_TO_SHELF;
            } else {
                return ADD_FAILURE;
            }
        }

        return BOOK_EXISTS_IN_SHELF;
    }
    public function getShelf($userid)
    {
        $stmt = $this->con->prepare("select * from books_in_shelf where user_id=?");
        $stmt->bind_param("i", $userid);


        $stmt->execute();
        $stmt->bind_result($id, $title, $author, $translator, $description, $genre, $price, $av_rating, $t_rating, $p_rating, $language, $published_d, $name, $percent, $state, $start_date, $finish_date);

        $shelves = array();


        while ($stmt->fetch()) {


            $book = array();
            $book['id'] = $id;

            $book['title'] = $title;
            $book['author_name'] = $author;
            $book['translator'] = $translator;
            $book['description'] = $description;
            $book['genre'] = $genre;
            $book['price'] = $price;
            $book['state'] = $state;
            $book['average_rating'] = $av_rating;
            $book['total_ratings'] =  $t_rating;
            $book['peak_rating'] = $p_rating;
            $book['language'] = $language;
            $book['published_date'] = $published_d;
            $book['app_lang'] = $name;
            $book['percent'] = $percent;
            $book['start_date'] = $start_date;
            $book['finish_date'] = $finish_date;

            array_push($shelves, $book);
        }


        // $book['peak_rating'] =$book_data['peak_rating'];
        // $book['language'] = $book_data['language'];
        return $shelves;
    }
    public function getShelfbyState($userid, $state)
    {
        $stmt = $this->con->prepare("call getShelfByState(?,?)");
        $stmt->bind_param("si", $state, $userid);


        $stmt->execute();
        $stmt->bind_result($id, $title, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings, $language, $percent, $start_date, $finish_date);

        $shelves = array();


        while ($stmt->fetch()) {

            $stmt->close();
            $book = array();
            $book['id'] = $id;

            $book['title'] = $title;
            $book['author_name'] = $author;

            $book['description'] = $description;
            $book['genre'] = $this->getGenres($id);
            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['no_of_ratings'] =  $no_of_ratings;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $language;
            $book['published_date'] = $published_d;
            $book['no_of_purchases'] = $no_of_purchases;
            $book['percent'] = $percent;
            $book['start_date'] = $start_date;
            $book['finish_date'] = $finish_date;

            array_push($shelves, $book);
        }


        // $book['peak_rating'] =$book_data['peak_rating'];
        // $book['language'] = $book_data['language'];
        return $shelves;
    }

    public function updateShelf($userid, $title, $state, $start_date, $finish_date, $percent)
    {
        $stmt = $this->con->prepare("call updateShelf(?,?,?,?,?,?)");

        $stmt->bind_param("sdsssi", $state, $percent, $start_date, $finish_date, $title, $userid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function deleteBookFromShelf($title, $userid)
    {
        $stmt = $this->con->prepare('call deleteFromShelf(?,?)');
        $stmt->bind_param('si', $title, $userid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function getAllBooks()
    {
        $stmt = $this->con->prepare("select * from book_data");

        $stmt->execute();
        $stmt->bind_result($id, $title, $author, $translator, $description, $genre, $price, $av_rating, $t_rating, $p_rating, $image, $file, $language, $published_d, $name);

        $books = array();
        while ($stmt->fetch()) {

            $book = array();
            $book['id'] = $id;

            $book['title'] = $title;
            $book['author_name'] = $author;
            $book['translator'] = $translator;
            $book['description'] = $description;
            $book['genre'] = $genre;
            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['total_ratings'] =  $t_rating;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $language;
            $book['published_date'] = $published_d;
            $book['app_lang'] = $name;
            array_push($books, $book);
        }


        // $book['peak_rating'] =$book_data['peak_rating'];
        // $book['language'] = $book_data['language'];
        return $books;
    }

    public function getBookByTitle($title)
    {

        $stmt = $this->con->prepare("call getBooksByTitle(?)");
        $stmt->bind_param("s", $title);
        $stmt->execute();
        $stmt->bind_result($id, $btitle, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings,  $name);
        $stmt->fetch();
        $stmt->close();
        $book = array();


        $book['id'] = $id;

        $genres = $this->getGenres($id);

        $book['title'] = $btitle;
        $book['author_name'] = $author;

        $book['description'] = $description;
        $book['genres'] = $genres;
        $book['price'] = $price;
        $book['average_rating'] = $av_rating;
        $book['published_date'] = $published_d;
        $book['no_of_purchases'] = $no_of_purchases;
        $book['no_of_ratings'] =  $no_of_ratings;
        $book['peak_rating'] = $p_rating;
        $book['image'] = $image;
        $book['file'] = $file;
        $book['language'] = $name;


        // $book['peak_rating'] =$book_data['peak_rating'];
        // $book['language'] = $book_data['language'];
        return $book;
    }
    public function getGenres($id)
    {

        $stmt2 = $this->con->prepare("call bookgenres(?)");
        $stmt2->bind_param("i", $id);
        $stmt2->execute();
        $stmt2->bind_result($genre);

        $genres = '';
        $i = 0;
        while ($stmt2->fetch()) {

            if ($i > 0) {
                $genres = $genres . ", ";
            }
            $genres = $genres . $genre;

            $i++;
        }


        return $genres;
    }
    public function addRateReview($userid, $title, $rating, $review, $has_spoiler)
    {
        $bookid = $this->getBookIdByTitle($title);

        $stmt = $this->con->prepare("call addRateReview(?,?,?,?,?)");
        $stmt->bind_param("iidsi", $bookid, $userid, $rating, $review, $has_spoiler);
        if ($stmt->execute()) {
            $this->addRatingValues($rating, $bookid);
            return true;
        }
        return false;
    }
    public function updateRateReview($userid, $title, $rating, $review, $has_spoiler)
    {
        $bookid = $this->getBookIdByTitle($title);


        $stmt = $this->con->prepare("call UpdateRateReview(?,?,?,?,?)");
        $stmt->bind_param("dsiii", $rating, $review, $has_spoiler, $userid, $bookid);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function removeRateReview($userid, $title)
    {
        $bookid = $this->getBookIdByTitle($title);


        $stmt = $this->con->prepare("call removeRateReview(?,?)");
        $stmt->bind_param("ii", $userid, $bookid);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function addComment($userid, $title, $comment, $has_spoiler)
    {
        $bookid = $this->getBookIdByTitle($title);

        $stmt = $this->con->prepare("call addComment(?,?,?,?)");
        $stmt->bind_param("iisi", $bookid, $userid, $comment, $has_spoiler);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function updateComent($userid, $title, $comment, $has_spoiler)
    {
        $bookid = $this->getBookIdByTitle($title);

        $stmt = $this->con->prepare("call updateComment(?,?,?,?");
        $stmt->bind_param("siii", $comment, $has_spoiler, $userid, $bookid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function removeComent($userid, $title)
    {
        $bookid = $this->getBookIdByTitle($title);

        $stmt = $this->con->prepare("call removeComment(?,?");
        $stmt->bind_param("ii", $userid, $bookid);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }


    public function search($key)
    {
        $stmt = $this->con->prepare("call search(?)");
        $stmt->bind_param("s", $key);
        $stmt->execute();
        $stmt->bind_result($id, $btitle, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings,  $name);

        $books = array();
        while ($stmt->fetch()) {

            $book = array();
            $book['id'] = $id;

            $book['title'] = $btitle;
            $book['author_name'] = $author;

            $book['description'] = $description;




            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['no_of_ratings'] =  $no_of_ratings;
            $book['no_of_purchases'] =  $no_of_purchases;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $name;
            $book['published_date'] = $published_d;

            array_push($books, $book);
        }


        return $books;
    }
    public function getAllGenres($lang)
    {
        $stmt = $this->con->prepare("call getAllGenres(?)");
        $stmt->bind_param('s', $lang);
        $stmt->execute();
        $stmt->bind_result($id, $genren);

        $genres = array();
        while ($stmt->fetch()) {

            $genre = array();
            $genre['id'] = $id;

            $genre['genre'] = $genren;


            array_push($genres, $genre);
        }



        return $genres;
    }
    public function getBookIdByTitle($title)
    {
        $stmt = $this->con->prepare("select getBookId(?)");
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        return $id;
    }

    public function isBookExistInShelf($title, $userid)
    {
        $id = $this->getBookIdByTitle($title);
        $stmt = $this->con->prepare("select getBookinShelf(?,?)");
        $stmt->bind_param("ii", $id, $userid);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows > 0;
    }
    public function addRatingValues($rating, $bookid)
    {

        $avr = $this->calculaAverageRating($bookid);

        $peakv = $this->getPeakRating($bookid);
        if ($rating > $peakv) {
            $peakv = $rating;
        }
        $stmt = $this->con->prepare("update books set average_rating=?, total_ratings=total_ratings + ?, peak_rating=?, no_of_ratings=no_of_ratings + 1 where  id=?");
        $stmt->bind_param("dddi", $avr, $rating, $peakv, $bookid);
        $stmt->execute();
    }
    public function updateRatingValues($rating, $bookid, $userid)
    {
        $oldr = $this->getCurrentRating($userid, $bookid);
        $avr = $this->calculaAverageRating($bookid);
        $peakv = $this->getPeakRating($bookid);
        if ($rating > $peakv) {
            $peakv = $rating;
        }
        $stmt = $this->con->prepare("update books set average_rating=?, total_ratings=total_ratings + ?, peak_rating=?, no_of_ratings=no_of_ratings + 1 where  book_id=?");
        $stmt->bind_param("dddi", $avr, $rating - $oldr, $peakv, $bookid);
        $stmt->execute();
    }
    public function getPeakRating($bookid)
    {
        $stmt = $this->con->prepare("select peak_rating from books where id=?");
        $stmt->bind_param("s", $bookid);
        $stmt->execute();
        $stmt->bind_result($peakrating);
        $stmt->fetch();
        return $peakrating;
    }

    public function calculaAverageRating($bookid)
    {
        $stmt = $this->con->prepare("select AVG(rating) from rating_reviews where book_id=?");
        $stmt->bind_param("s", $bookid);
        $stmt->execute();
        $stmt->bind_result($avrating);
        $stmt->fetch();

        return $avrating;
    }
    public function getCurrentRating($userid, $bookid)
    {
        $stmt = $this->con->prepare("select rating from rating_reviews where book_id=? and user_id=?");
        $stmt->bind_param("ii", $bookid, $userid);
        $stmt->execute();
        $stmt->bind_result($rating);
        $stmt->fetch();
        return $rating;
    }
    public function getLanguage($lang)
    {
        $stmt = $this->con->prepare("select id from app_languages where name=?");
        $stmt->bind_param("s", $lang);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        return $id;
    }
    public function getGenreId($genre)
    {
        $stmt = $this->con->prepare("select getGenreByName(?)");
        $stmt->bind_param("s", $genre);
        $stmt->execute();
        $stmt->bind_result($genre);
        $stmt->fetch();
        return $genre;
    }

    //Recommendations

    public function getGenreSimilarBooks($genre_names, $langname)
    {
        $genres = "[";
        foreach ($genre_names as $genre) {
            $g = $this->getGenreId($genre);
            if ($genres == "[") {
                $genres = $genres . "" . $g;
            } else {
                $genres = $genres . "," . $g;
            }
        }
        $genres = $genres . "]";
        $lang = $this->getLanguage($langname);


        $python = `python -c "import recommendor as r; a= $genres; b= $lang;m=r.suggestbygenre(a,b); print(m);"`;

        $python = str_replace("[", "", $python);
        $python = str_replace("]", "", $python);
        $arr = explode(",", $python);

        $books = array();
        foreach ($arr as $a) {
            $stmt = $this->con->prepare("call getBooksById(?)");

            $stmt->bind_param("i", $a);

            $stmt->execute();
            $stmt->bind_result($id, $btitle, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings, $language);

            $stmt->fetch();

            $book = array();
            $book['id'] = $id;

            $book['title'] = $btitle;
            $book['author_name'] = $author;

            $book['description'] = $description;




            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['no_of_ratings'] =  $no_of_ratings;
            $book['no_of_purchases'] =  $no_of_purchases;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $language;
            $book['published_date'] = $published_d;
            $stmt->close();
            $book['genres'] = $this->getGenres($id);
            array_push($books, $book);
        }

        return $books;
    }
    public function bestSellers($lang)
    {
        $stmt = $this->con->prepare("call bestSellers(?)");
        $stmt->bind_param("s", $lang);
        $stmt->execute();
        $stmt->bind_result($id, $btitle, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings,  $name);

        $books = array();
        while ($stmt->fetch()) {

            $book = array();
            $book['id'] = $id;

            $book['title'] = $btitle;
            $book['author_name'] = $author;

            $book['description'] = $description;




            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['no_of_ratings'] =  $no_of_ratings;
            $book['no_of_purchases'] =  $no_of_purchases;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $name;
            $book['published_date'] = $published_d;


            array_push($books, $book);
        }

        $stmt->close();
        return $books;
    }
    public function popular()
    {
        $stmt = $this->con->prepare("call popular()");

        $stmt->execute();
        $stmt->bind_result($id, $btitle, $author, $description, $price, $av_rating, $published_d, $no_of_purchases, $p_rating, $image, $file, $no_of_ratings,  $name);

        $books = array();
        while ($stmt->fetch()) {
            $stmt->close();
            $book = array();
            $book['id'] = $id;

            $book['title'] = $btitle;
            $book['author_name'] = $author;

            $book['description'] = $description;




            $book['price'] = $price;
            $book['average_rating'] = $av_rating;
            $book['no_of_ratings'] =  $no_of_ratings;
            $book['no_of_purchases'] =  $no_of_purchases;
            $book['peak_rating'] = $p_rating;
            $book['image'] =  $image;
            $book['file'] = $file;
            $book['language'] = $name;
            $book['published_date'] = $published_d;


            array_push($books, $book);
        }


        return $books;
    }
}
