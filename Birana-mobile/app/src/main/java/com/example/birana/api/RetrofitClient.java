
package com.example.birana.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
  private static final String baseUrl = "http://192.168.43.148/MyApi/public/";
  private static RetrofitClient mInstance;
  private Retrofit retrofit;
  private RetrofitClient(){
    retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
  }
  public static synchronized RetrofitClient getInstance(){
    if (mInstance == null){
      mInstance = new RetrofitClient();
    }
    return mInstance;
  }
  public Api getApi(){
    return retrofit.create(Api.class);
  }
}
