package com.example.birana.activities;

import android.os.Bundle;

import android.util.Log;

import com.example.birana.R;
import com.example.birana.adapters.ShelvesAdapter;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.Shelf;
import com.example.birana.models.ShelfResponse;
import com.example.birana.models.User;
import com.example.birana.storage.SharedPrefManager;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentlyReadingActivity extends AppCompatActivity {

  private RecyclerView mRecyclerView;
  private List<Shelf> shelflist;
  private ShelvesAdapter adapter;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_chosen_shelf);
    mRecyclerView = findViewById(R.id.recycler);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    User user = SharedPrefManager.getInstance(CurrentlyReadingActivity.this).getUser();
    Call<ShelfResponse> call = RetrofitClient.getInstance().getApi().getshelf(1, "CR", "English");
    call.enqueue(new Callback<ShelfResponse>() {
      @Override
      public void onResponse(Call<ShelfResponse> call, Response<ShelfResponse> response) {
        shelflist= response.body().getShelf();

        adapter = new ShelvesAdapter(CurrentlyReadingActivity.this, shelflist);
        mRecyclerView.setAdapter(adapter);
      }

      @Override
      public void onFailure(Call<ShelfResponse> call, Throwable t) {

      }
    });

  }



}
