package com.example.birana.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birana.R;
import com.example.birana.models.Genre;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreViewHolder>{
    private Context mctx;
    private List<Genre> genrelist;

    public GenreAdapter(Context mctx, List<Genre> genrelist) {
        this.mctx = mctx;
        this.genrelist = genrelist;
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mctx).inflate(R.layout.recycler_genrelist, parent, false);

        return new GenreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {
    Genre genre = genrelist.get(position);
    holder.but.setText(genre.getGenre());

    }

    @Override
    public int getItemCount() {
        return genrelist.size();
    }

    public  class GenreViewHolder extends RecyclerView.ViewHolder{
        private Chip but;

        public GenreViewHolder(@NonNull View itemView) {
            super(itemView);
            but = itemView.findViewById(R.id.genrelist);

        }
    }
}
