package com.example.birana.activities;

import android.content.Intent;
import android.os.Handler;

import android.os.Bundle;

import com.example.birana.R;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        int delay = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(MainActivity.this, SignUpActivity.class));
                finish();
            }
        }, delay*1000);

    }



}
