package com.example.birana.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.birana.models.Book;
import com.example.birana.models.User;

import java.util.List;

public class SharedPrefManager {
    private static final String SHARED_PREF_NAME = "my_shared_pref";
    private static SharedPrefManager mInstance;
    private Context mctx;

    private SharedPrefManager(Context mctx) {
        this.mctx = mctx;
    }

    public static synchronized SharedPrefManager getInstance(Context mctx) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(mctx);
        }
        return mInstance;
    }

    public void saveUser(User user) {
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("id", user.getId());
        editor.putString("email", user.getEmail());
        editor.putInt("crcount", user.getCrcount());
        editor.putInt("wrcount", user.getWrcount());
        editor.putInt("rcount", user.getCrcount());
        editor.putString("name", user.getName());
        editor.putInt("log_status", user.getLog_status());


        editor.apply();


    }
    public void addLanguage(String lang){
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("language", lang);
        editor.apply();
    }

    public void saveBook(Book book){
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("id", book.getBookid());
        editor.putString("title", book.getTitle());

        editor.putString("author_name", book.getAuthor_name());
        editor.putString("translator", book.getTranslator());
        editor.putString("description", book.getDescription());
        editor.putString("genre", book.getGenre());

        editor.putFloat("price", book.getPrice().floatValue());
        editor.putFloat("average_rating", book.getAverage_rating().floatValue());
        editor.putFloat("total_ratings", book.getTotal_ratings().floatValue());
        editor.putFloat("peak_rating", book.getPeak_rating().floatValue());
        editor.putString("language", book.getLanguage());
        editor.putString("published_date", book.getPublished_date());
        editor.putString("app_lang", book.getApp_lang());
        editor.putString("image", book.getImage());
        editor.putString("file", book.getFile());

        editor.apply();

    }
    public String getLanguage(){
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String lang = sharedPreferences.getString("language", null);
        return lang;
    }
public Book getBook(){
    SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    Book book = new Book(sharedPreferences.getInt("id", -1), sharedPreferences.getString("title", null)
            , sharedPreferences.getString("author_name", null), sharedPreferences.getString("translator", null),
            sharedPreferences.getString("description", null),sharedPreferences.getString("genre", null),
            sharedPreferences.getFloat("price", -1) ,  sharedPreferences.getFloat("average_rating", -1),
            sharedPreferences.getFloat("total_ratings", -1), sharedPreferences.getFloat("peak_rating", -1),
            sharedPreferences.getString("language", null), sharedPreferences.getString("published_date", null),
            sharedPreferences.getString("app_lang", null), sharedPreferences.getString("image", null),
            sharedPreferences.getString("file", null)
           );
        return book;
}

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("id", -1) != -1;
    }

    public User getUser() {
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        User user = new User(sharedPreferences.getInt("id", -1), sharedPreferences.getString("email", null)
                , sharedPreferences.getString("name", null), sharedPreferences.getInt("crcount", -1)
                , sharedPreferences.getInt("wrcount", -1), sharedPreferences.getInt("rcount", -1),
                sharedPreferences.getInt("log_status", -1));
        return user;

    }
    public void clear(){
        SharedPreferences sharedPreferences = mctx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
