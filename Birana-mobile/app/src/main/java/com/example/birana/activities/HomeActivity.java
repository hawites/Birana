package com.example.birana.activities;



import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import android.view.MenuItem;

import com.example.birana.R;
import com.example.birana.storage.SharedPrefManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

//implement the interface OnNavigationItemSelectedListener in your activity class
public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

Context ctx;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_home);

    //loading the default fragment
    loadFragment(new HomeFragment());

    //getting bottom navigation view and attaching the listener
    BottomNavigationView navigation = findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(this);

  }


  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    Fragment fragment = null;

    switch (item.getItemId()) {
      case R.id.navigation_shelf:
        fragment = new HomeFragment();
        break;

      case R.id.navigation_store:
        fragment = new StoreFragment();
        break;

      case R.id.navigation_books:
        fragment = new MyBooksFragment();
        break;

      case R.id.navigation_profile:
        fragment = new ProfileFragment();
        break;
    }

    return loadFragment(fragment);
  }

  private boolean loadFragment(Fragment fragment) {
    //switching fragment
    if (fragment != null) {
      getSupportFragmentManager()
              .beginTransaction()
              .replace(R.id.fragment_container, fragment)
              .commit();
      return true;
    }
    return false;
  }

}