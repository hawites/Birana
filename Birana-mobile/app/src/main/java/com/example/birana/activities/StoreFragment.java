package com.example.birana.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.birana.R;
import com.example.birana.adapters.SAdapter;
import com.example.birana.api.RetrofitClient;
import com.example.birana.helper.LanguageSupport;
import com.example.birana.models.Book;
import com.example.birana.models.BookResponse;
import com.example.birana.storage.SharedPrefManager;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StoreFragment extends Fragment {
  private List<Book> books;
  private RecyclerView recyclerview;

  private SAdapter sadapter;

  private ProgressBar mProgressBar;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_store,container, false);
//    String lang = SharedPrefManager.getInstance(getActivity()).getLanguage();
//
//    LanguageSupport ls = new LanguageSupport();
//    ls.changeLocale(lang, getActivity());
//    setUpToolbar(view);
//    mProgressBar = view.findViewById(R.id.progress);
//    recyclerview = view.findViewById(R.id.recycler);
//
//    recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
//    recyclerview.setHasFixedSize(true);
//    fetchBooks("");
    return view;
  }

  private void setUpToolbar(View view) {
    Toolbar toolbar = view.findViewById(R.id.app_bar);
    AppCompatActivity activity = (AppCompatActivity) getActivity();
    if (activity != null) {
      activity.setSupportActionBar(toolbar);
    }
  }
  public void fetchBooks(String key){
    Call<BookResponse> call = RetrofitClient.getInstance().getApi().getSearchResult(key, "English");
    call.enqueue(new Callback<BookResponse>() {
      @Override
      public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
        mProgressBar.setVisibility(View.GONE);
        books = response.body().getBook();

        sadapter = new SAdapter(books,getActivity());
        recyclerview.setAdapter(sadapter);
        sadapter.notifyDataSetChanged();

      }

      @Override
      public void onFailure(Call<BookResponse> call, Throwable t) {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), "Error on" + t.toString(), Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
    menuInflater.inflate(R.menu.home_menu, menu);


    SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
    final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
    searchView.setSearchableInfo(
            searchManager.getSearchableInfo(getActivity().getComponentName()));
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {

        fetchBooks(query);


        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        fetchBooks(newText);
        return false;
      }
    });
    searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {

        if(!hasFocus){
          fetchBooks("");
          InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        }
      }
    });
    super.onCreateOptionsMenu(menu, menuInflater);
  }

}