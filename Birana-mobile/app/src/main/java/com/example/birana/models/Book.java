
package com.example.birana.models;

public class Book {
  private int id;
  private String title;
  private String author_name;
  private String translator;
  private String description;
  private String genre;
  private Float price;
  private Float average_rating, total_ratings, peak_rating;
  private String language;
  private String published_date;
  private String app_lang;
  private String image;
  private String file;

  public Book(int id, String title, String author_name, String translator, String description, String genre, Float price, Float average_rating, Float total_ratings, Float peak_rating, String language, String published_date, String app_lang, String image, String file) {
    this.id = id;
    this.title = title;
    this.author_name = author_name;
    this.translator = translator;
    this.description = description;
    this.genre = genre;
    this.price = price;
    this.average_rating = average_rating;
    this.total_ratings = total_ratings;
    this.peak_rating = peak_rating;
    this.language = language;
    this.published_date = published_date;
    this.app_lang = app_lang;
    this.image = image;
    this.file = file;
  }

  public String getImage() {
    return image;
  }

  public String getFile() {
    return file;
  }

  public int getBookid() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getAuthor_name() {
    return author_name;
  }

  public String getTranslator() {
    return translator;
  }

  public String getDescription() {
    return description;
  }

  public String getGenre() {
    return genre;
  }

  public Float getPrice() {
    return price;
  }

  public Float getAverage_rating() {
    return average_rating;
  }

  public Float getTotal_ratings() {
    return total_ratings;
  }

  public Float getPeak_rating() {
    return peak_rating;
  }

  public int getId() {
    return id;
  }

  public String getLanguage() {
    return language;
  }

  public String getPublished_date() {
    return published_date;
  }

  public String getApp_lang() {
    return app_lang;
  }
}

