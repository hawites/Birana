package com.example.birana.models;

public class User {
    private int id, crcount,wrcount,rcount,log_status;
    private String email, name;

    public User(int id, String email, String name, int crcount, int wrcount, int rcount, int log_status) {
        this.id = id;
        this.crcount = crcount;
        this.wrcount = wrcount;
        this.rcount = rcount;
        this.log_status = log_status;
        this.email = email;
        this.name = name;

    }

    public int getId() {
        return id;
    }

    public int getCrcount() {
        return crcount;
    }

    public int getWrcount() {
        return wrcount;
    }

    public int getRcount() {
        return rcount;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public int getLog_status() {
        return log_status;
    }


}
