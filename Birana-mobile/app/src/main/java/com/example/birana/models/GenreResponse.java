package com.example.birana.models;

import java.util.List;

public class GenreResponse {
    private boolean error;
    private List<Genre> genre;

    public GenreResponse(boolean error, List<Genre> genre) {
        this.error = error;
        this.genre = genre;
    }

    public boolean isError() {
        return error;
    }

    public List<Genre> getGenre() {
        return genre;
    }
}
