package com.example.birana.models;

import java.util.List;

public class BookResponse {
  private boolean error;
  private List<Book> book;

  public BookResponse(boolean error, List<Book> book) {
    this.error = error;
    this.book = book;
  }

  public boolean isError() {
    return error;
  }

  public List<Book> getBook() {
    return book;
  }
}
