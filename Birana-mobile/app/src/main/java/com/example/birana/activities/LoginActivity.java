
package com.example.birana.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.birana.R;




import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;


import com.example.birana.api.RetrofitClient;
import com.example.birana.models.LoginResponse;
import com.example.birana.storage.SharedPrefManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements ValidationListener {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    @NotEmpty
    @Email
    private EditText em;

    @NotEmpty
    private EditText pas;
    String semail, spassword;

    private Button lbutton;
    private Validator validator;

    private Button btnLogin;
    private Button btnLinkToRegister;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);



        TextView signup = (TextView) findViewById(R.id.signupl);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
        em = findViewById(R.id.email);

        pas = findViewById(R.id.password);
        lbutton = (Button) findViewById(R.id.logbutton);
        lbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = em.getText().toString().trim();
                setEmail(email);
                String password = pas.getText().toString().trim();
                setPassword(password);
                validator.validate();


            }
        });


    }


    public void setEmail(String email){
        this.semail = email;

    }
    public void setPassword (String password){
        this.spassword = password;
    }

    @Override
    public void onValidationSucceeded() {

        checkLogin(semail, spassword);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for(ValidationError error: errors){
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if(view instanceof EditText){
                ((EditText) view).setError(message);
            }
            else{
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }
    }

    public void checkLogin(String email, String password) {

    String status;
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(email, password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse lr = response.body();
                Log.i("nn",lr + "");
                if(!lr.isError()){
                    SharedPrefManager.getInstance(LoginActivity.this).saveUser(lr.getUser());
                    Log.i("mmm", lr.getUser().getLog_status() + "");


                    if(lr.getUser().getLog_status() == 1){
                        Intent intent = new Intent(LoginActivity.this, LanguageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    else if(lr.getUser().getLog_status() == 2){
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }


                }
                else{
                    Toast.makeText(LoginActivity.this, lr.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
}
