package com.example.birana.helper;


import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class LanguageSupport {
    private static Locale myLocale;

    public void changeLocale(String lang, Context ctx) {
        if (lang.equalsIgnoreCase(""))
            return;

        myLocale = new Locale(lang);//Set Selected Locale

        Locale.setDefault(myLocale);//set new locale as default

        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        ctx.getResources().updateConfiguration(config,  ctx.getResources().getDisplayMetrics());//Update the config

    }
}
