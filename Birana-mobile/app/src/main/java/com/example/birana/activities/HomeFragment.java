package com.example.birana.activities;



import com.example.birana.R;
import com.example.birana.adapters.SAdapter;
import com.example.birana.api.RetrofitClient;
import com.example.birana.helper.EncryptFile;
import com.example.birana.helper.LanguageSupport;
import com.example.birana.models.Book;
import com.example.birana.models.BookResponse;


import com.example.birana.models.User;
import com.example.birana.storage.SharedPrefManager;
import com.google.android.material.button.MaterialButton;


import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    private TextView read_count;
    private TextView cread_count;
    private TextView wread_count;
    private MaterialButton cshelf;

    private List<Book> books;
    private RecyclerView recyclerview;
    private static Locale myLocale;
    private SAdapter sadapter;
    private CardView card;
    private ProgressBar mProgressBar;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment with the ProductGrid theme
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //String lang = SharedPrefManager.getInstance(getActivity()).getLanguage();
        String lang="en";
        LanguageSupport ls = new LanguageSupport();
        ls.changeLocale(lang, getActivity());


//        // Set up the toolbar
        setUpToolbar(view);


        mProgressBar = view.findViewById(R.id.progress);
        recyclerview = view.findViewById(R.id.recycler);
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview.setHasFixedSize(true);

        card = view.findViewById(R.id.card1);

        card.setVisibility(View.VISIBLE);




        User user = SharedPrefManager.getInstance(getActivity()).getUser();
        read_count = view.findViewById(R.id.read_count);


        read_count.setText("Read (" +user.getRcount()+ ")");
        Log.i("n",user.getName() + "");
        if(user.getRcount()==0){
            read_count.setClickable(false);
            read_count.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        cread_count = view.findViewById(R.id.cread_count);
        cread_count.setText("Currently reading (" +user.getCrcount() + ")");
        if(user.getCrcount()==0){
            cread_count.setClickable(false);
            cread_count.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        wread_count = view.findViewById(R.id.wread_count);
        wread_count.setText("Want to Read (" +user.getWrcount()+ ")");
        Log.i("n",user.getRcount()+ "" );

        if(user.getWrcount()==0){
            wread_count.setClickable(false);
            wread_count.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        if(cread_count.isClickable()){
            cread_count.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), CurrentlyReadingActivity.class));
                }
            });}
//        cshelf = view.findViewById(R.id.customshelf);
//        cshelf.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
//                View mView = layoutInflaterAndroid.inflate(R.layout.add_custom_shelf_dialog_box, null);
//                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
//                alertDialogBuilderUserInput.setView(mView);
//
//                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
//                alertDialogBuilderUserInput
//                        .setCancelable(false)
//                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialogBox, int id) {
//                                // ToDo get user input here
//                            }
//                        })
//
//                        .setNegativeButton("Cancel",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialogBox, int id) {
//                                        dialogBox.cancel();
//                                    }
//                                });
//
//                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
//                alertDialogAndroid.show();
//            }
//        });


        return view;
    }

    public void fetchBooks(String key){
        Call<BookResponse> call = RetrofitClient.getInstance().getApi().getSearchResult(key, "English");
        call.enqueue(new Callback<BookResponse>() {
            @Override
            public void onResponse(Call<BookResponse> call, Response<BookResponse> response) {
                mProgressBar.setVisibility(View.GONE);
                books = response.body().getBook();
                card.setVisibility(View.GONE);
                sadapter = new SAdapter(books,getActivity());
                recyclerview.setAdapter(sadapter);
                sadapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<BookResponse> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error on" + t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void setUpToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.app_bar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.setSupportActionBar(toolbar);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.home_menu, menu);


        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchview = (SearchView) menu.findItem(R.id.search).getActionView();
        searchview.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName())
        );
        searchview.setIconifiedByDefault(false);
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("nn", query);
                fetchBooks(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fetchBooks(newText);
                return false;
            }
        });
        searchview.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchview.getWindowToken(), 0);
                    card.setVisibility(View.VISIBLE);
                }
            }
        });


        super.onCreateOptionsMenu(menu, menuInflater);
    }
    public void changeLocale(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;

        myLocale = new Locale(lang);//Set Selected Locale

        Locale.setDefault(myLocale);//set new locale as default

        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        getActivity().getBaseContext().getResources().updateConfiguration(config,  getActivity().getBaseContext().getResources().getDisplayMetrics());//Update the config

    }

}
