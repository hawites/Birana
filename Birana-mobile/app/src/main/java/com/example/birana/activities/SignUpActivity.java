package com.example.birana.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;


import com.example.birana.R;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.DefaultResponse;
import com.example.birana.storage.SharedPrefManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private Button btnRegister;

    @NotEmpty
    @Length(min=2,max=50)
    private EditText inputFullName;

    @NotEmpty
    @Email
    private EditText inputEmail;

    @NotEmpty
    @Length(min=8,max=20)
    @Password
    private EditText inputPassword;

    @NotEmpty

    @ConfirmPassword
    private EditText confirmpass;
    private String newname, nemail,npassword;
    private ProgressDialog pDialog;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_signup);

        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.nemail);
        inputPassword = (EditText) findViewById(R.id.newpass);
        confirmpass = (EditText) findViewById(R.id.confirmpass);

        validator = new Validator(this);
        validator.setValidationListener(this);

        TextView login = (TextView) findViewById(R.id.loginl);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });



        btnRegister = (Button) findViewById(R.id.signbutton);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputFullName.getText().toString().trim();
                setName(name);
                String email = inputEmail.getText().toString().trim();
                setEmail(email);
                String password = inputPassword.getText().toString().trim();
                setPassword(password);
                validator.validate();



            }
        });

    }
    public void setEmail(String email){
        this.nemail = email;

    }
    public void setPassword (String password){
        this.npassword = password;
    }
    public void setName (String name){
        this.newname = name;
    }
    public String getEmail(){
        return this.nemail;

    }
    public String getPassword (){
        return this.npassword;
    }
    public String getName (){
        return this.newname;
    }

    @Override
    public void onValidationSucceeded() {

        registerUser(getName(), getEmail(), getPassword());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for(ValidationError error: errors){
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if(view instanceof EditText){
                ((EditText) view).setError(message);
            }
            else{
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            startActivity(new Intent(SignUpActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

        }
    }
    private void registerUser(String name, String email, String password) {
        Call<DefaultResponse> call = RetrofitClient
                                .getInstance()
                                .getApi()
                                .createUser(name,email,password);

        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {


                DefaultResponse dr = response.body();
                if(!dr.isError()){

                        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                        Toast.makeText(SignUpActivity.this, dr.getMessage(), Toast.LENGTH_LONG).show();
                }
                else{

                        Toast.makeText(SignUpActivity.this, dr.getMessage(), Toast.LENGTH_LONG).show();

                    }

            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
