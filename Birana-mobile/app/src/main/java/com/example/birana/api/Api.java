package com.example.birana.api;


import com.example.birana.models.BookResponse;
import com.example.birana.models.DefaultResponse;
import com.example.birana.models.GenreResponse;
import com.example.birana.models.LoginResponse;
import com.example.birana.models.ShelfResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Api {
    @FormUrlEncoded
    @POST("createuser")
    Call<DefaultResponse> createUser(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("userlogin")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("getshelf")
    Call<ShelfResponse> getshelf(
            @Query("userid") int userid,
            @Query("state") String state,
            @Query("lang") String lang
    );
    @GET("getcustomshelf")
    Call<ShelfResponse> getCustomShelf(
            @Query("userid") int userid,

            @Query("lang") String lang
    );

    @GET("search")
    Call<BookResponse> getSearchResult(
            @Query("key") String key,
            @Query("lang") String lang
    );
    @FormUrlEncoded
    @POST("addtoshelf")
    Call<LoginResponse> addToShelf(
            @Field("userid") int userid,
            @Field("title") String title,
            @Field("state") String state,
            @Field("lang") String lang,
            @Field("percent") Double percent,
            @Field("start_date") String start_date,
            @Field("finish_date") String finish_date
    );
    @GET("getgenre")
    Call<GenreResponse> getgenre(
            @Query("lang") String lang
    );

}
