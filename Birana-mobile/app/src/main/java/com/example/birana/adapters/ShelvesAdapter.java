package com.example.birana.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.birana.R;
import com.example.birana.activities.SingleBookViewActivity;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.Book;
import com.example.birana.models.Shelf;
import com.example.birana.storage.SharedPrefManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ShelvesAdapter extends RecyclerView.Adapter<ShelvesAdapter.ShelvesViewHolder> {
  private Context mctx;
  private List<Shelf> booklist;
  private static final String baseUrl = "http://192.168.43.148/";

  public ShelvesAdapter(Context mctx, List<Shelf> booklist) {
    this.mctx = mctx;
    this.booklist = booklist;
  }

  @NonNull
  @Override
  public ShelvesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mctx).inflate(R.layout.recycleview_shelf, parent, false);

    return new ShelvesViewHolder(view);

  }

  @Override
  public void onBindViewHolder(@NonNull ShelvesViewHolder holder, final int position) {

    Shelf shelf =booklist.get(position);
    holder.title.setText(shelf.getTitle());

    holder.author.setText(shelf.getAuthor_name());
    holder.averager.setText(shelf.getAverage_rating().toString());
    holder.totalr.setText(shelf.getTotal_ratings().toString());
    Picasso.get().load(baseUrl + shelf.getImageurl()).into(holder.img);
    holder.linear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

//                SharedPrefManager.getInstance().saveBook(books.get(position));
//                v.getContext().startActivity(new Intent(mctx, SingleBookViewActivity.class));
      }
    });

  }

  @Override
  public int getItemCount() {
    return booklist.size();
  }

  class ShelvesViewHolder extends RecyclerView.ViewHolder{
    TextView title, author, totalr, averager;
    ImageView img;
    LinearLayout linear;
    public ShelvesViewHolder(@NonNull View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.title_c);
      author = itemView.findViewById(R.id.authorc);
      totalr = itemView.findViewById(R.id.tratingc);
      averager = itemView.findViewById(R.id.avratingc);
      img = itemView.findViewById(R.id.bimgc);
      linear = itemView.findViewById(R.id.linearc);
    }
  }
}
