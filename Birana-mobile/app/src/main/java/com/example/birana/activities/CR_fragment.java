package com.example.birana.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birana.R;
import com.example.birana.adapters.ShelvesAdapter;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.Shelf;
import com.example.birana.models.ShelfResponse;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CR_fragment extends Fragment {
  private RecyclerView mRecyclerView;
  private List<Shelf> shelflist;
  private ShelvesAdapter adapter;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    //just change the fragment_dashboard
    //with the fragment you want to inflate
    //like if the class is HomeFragment it should have R.layout.home_fragment
    //if it is DashboardFragment it should have R.layout.fragment_dashboard
    View view = inflater.inflate(R.layout.layout_chosen_shelf, null);




    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mRecyclerView = view.findViewById(R.id.recycler);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    Call<ShelfResponse> call = RetrofitClient.getInstance().getApi().getshelf(1, "CR", "English");
    call.enqueue(new Callback<ShelfResponse>() {
      @Override
      public void onResponse(Call<ShelfResponse> call, Response<ShelfResponse> response) {
        shelflist= response.body().getShelf();
        adapter = new ShelvesAdapter(getActivity(), shelflist);
        mRecyclerView.setAdapter(adapter);
      }

      @Override
      public void onFailure(Call<ShelfResponse> call, Throwable t) {

      }
    });
  }
}
