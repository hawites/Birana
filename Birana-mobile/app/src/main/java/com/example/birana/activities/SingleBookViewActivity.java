package com.example.birana.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.birana.R;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.Book;
import com.example.birana.models.LoginResponse;
import com.example.birana.models.User;
import com.example.birana.storage.SharedPrefManager;
import com.squareup.picasso.Picasso;

public class SingleBookViewActivity extends AppCompatActivity {
  private TextView booktext, author, avrating, desc, av_r;
  private EditText sdate, fdate, progress;
  private RadioButton read, cread, wtread;
  private ImageView imgv;
  private Double val;
  private Button toShelf, viewm,remove;
  private String btntext= "Add to shelf", state="R";

  private static final String baseUrl = "http://192.168.43.148/";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_book_view);
    final Book book= SharedPrefManager.getInstance(this).getBook();
    booktext = findViewById(R.id.stitle_r);
    booktext.setText(book.getTitle());
    author = findViewById(R.id.sauthorn);
    author.setText(book.getAuthor_name());
    avrating=findViewById(R.id.avrating);
    avrating.setText(book.getAverage_rating().toString());
    desc= findViewById(R.id.sdesc_r);
    desc.setText(book.getDescription());
    av_r=findViewById(R.id.av_r);
    av_r.setText(book.getAverage_rating().toString());
    imgv = findViewById(R.id.simg);
    Picasso.get().load(baseUrl + book.getImage()).into(imgv);


    makeTextViewResizable(desc, 4, "View More", true);
    toShelf = findViewById(R.id.toshelf);
    toShelf.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(SingleBookViewActivity.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.add_to_shelf, null);
        sdate = mView.findViewById(R.id.start_date);
        fdate = mView.findViewById(R.id.finish_date);
        progress= mView.findViewById(R.id.progress);

        read = mView.findViewById(R.id.readrb);
        cread = mView.findViewById(R.id.crrb);
        wtread = mView.findViewById(R.id.wtrrb);
        remove =mView.findViewById(R.id.removeicon);
        if(toShelf.getText().toString() == "Add to shelf"){
          remove.setVisibility(View.GONE);

        }
        else{
          remove.setVisibility(View.VISIBLE);
        }

        RadioGroup rg = (RadioGroup) mView.findViewById(R.id.radiog);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
          public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch(checkedId){
              case R.id.readrb:
                sdate.setVisibility(View.VISIBLE);
                fdate.setVisibility(View.VISIBLE);
                progress.setVisibility(View.VISIBLE);
                state ="R";
                // do operations specific to this selection
                break;
              case R.id.crrb:
                sdate.setVisibility(View.VISIBLE);
                fdate.setVisibility(View.VISIBLE);
                progress.setVisibility(View.VISIBLE);
                state="CR";
                // do operations specific to this selection
                break;
              case R.id.wtrrb:
                sdate.setVisibility(View.GONE);
                fdate.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);

                state="WR";
                // do operations specific to this selection
                break;
            }
          }
        });


        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(SingleBookViewActivity.this);
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                  public void onClick(final DialogInterface dialogBox, int id) {
                    Drawable img = SingleBookViewActivity.this.getResources().getDrawable( R.drawable.ic_outline_arrow_drop_down );
                    img.setBounds( 0, 0, 60, 60 );
                    switch (state){
                      case "R":
                        toShelf.setText("Read");
                        toShelf.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                        toShelf.setCompoundDrawables( null, null, null, img );
                        break;
                      case "WR":
                        toShelf.setText("Want to Read");
                        toShelf.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                        toShelf.setCompoundDrawables( null, null, null, img );
                        break;
                      case "CR":
                        toShelf.setText("Currently Reading");
                        toShelf.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                        toShelf.setCompoundDrawables( null, null, null, img );
                        break;
                      default:

                    }

                    User user = SharedPrefManager.getInstance(SingleBookViewActivity.this).getUser();

                    Call<LoginResponse> call = RetrofitClient.getInstance().getApi().addToShelf(1, book.getTitle(),state,"English",0.0, "","");
                    call.enqueue(new Callback<LoginResponse>() {
                      @Override
                      public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        LoginResponse lr = response.body();
                        if(!lr.isError()){
                          SharedPrefManager.getInstance(SingleBookViewActivity.this).saveUser(lr.getUser());
                          dialogBox.dismiss();
                        }
                        else{
                          Toast.makeText(SingleBookViewActivity.this, lr.getMessage(), Toast.LENGTH_LONG).show();
                        }
                      }

                      @Override
                      public void onFailure(Call<LoginResponse> call, Throwable t) {

                      }
                    });

                  }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                          public void onClick(DialogInterface dialogBox, int id) {
                            dialogBox.cancel();
                          }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
      }
    });



  }
  private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                          final int maxLine, final String spanableText, final boolean viewMore) {
    String str = strSpanned.toString();
    SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

    if (str.contains(spanableText)) {


      ssb.setSpan(new MySpannable(false){
        @Override
        public void onClick(View widget) {
          tv.setLayoutParams(tv.getLayoutParams());
          tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
          tv.invalidate();
          if (viewMore) {
            makeTextViewResizable(tv, -1, "View Less", false);
          } else {
            makeTextViewResizable(tv, 3, "View More", true);
          }
        }
      }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

    }
    return ssb;

  }
  public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

    if (tv.getTag() == null) {
      tv.setTag(tv.getText());
    }
    ViewTreeObserver vto = tv.getViewTreeObserver();
    vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

      @SuppressWarnings("deprecation")
      @Override
      public void onGlobalLayout() {
        String text;
        int lineEndIndex;
        ViewTreeObserver obs = tv.getViewTreeObserver();
        obs.removeGlobalOnLayoutListener(this);
        if (maxLine == 0) {
          lineEndIndex = tv.getLayout().getLineEnd(0);
          text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
        } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
          lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
          text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
        } else {
          lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
          text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
        }
        tv.setText(text);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(
                addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                        viewMore), TextView.BufferType.SPANNABLE);
      }
    });

  }
}
