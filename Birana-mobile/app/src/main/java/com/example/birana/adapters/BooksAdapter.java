package com.example.birana.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.birana.R;
import com.example.birana.models.Book;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BooksViewHolder> {
  private Context mctx;
  private List<Book> booklist;

  @NonNull
  @Override
  public BooksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mctx).inflate(R.layout.fragment_store, parent, false);

    return new BooksViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull BooksViewHolder holder, int position) {
    Book book = booklist.get(position);
  }

  @Override
  public int getItemCount() {
    return booklist.size();
  }


  public class BooksViewHolder extends RecyclerView.ViewHolder{


    public BooksViewHolder(@NonNull View itemView) {
      super(itemView);
    }
  }
}
