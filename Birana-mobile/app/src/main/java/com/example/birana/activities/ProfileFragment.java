package com.example.birana.activities;


import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.birana.R;
import com.example.birana.models.User;
import com.example.birana.storage.SharedPrefManager;

import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;



public class ProfileFragment extends Fragment {
  private TextView textView, btnlogout;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    //just change the fragment_dashboard
    //with the fragment you want to inflate
    //like if the class is HomeFragment it should have R.layout.home_fragment
    //if it is DashboardFragment it should have R.layout.fragment_dashboard
    View view = inflater.inflate(R.layout.fragment_profile, null);
    textView = view.findViewById(R.id.usern);

    User user = SharedPrefManager.getInstance(this.getContext()).getUser();

    textView.setText(user.getName());
    btnlogout =view.findViewById(R.id.btnlogout);
    btnlogout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        logout();
      }
    });

    return view;
  }

  private void logout(){
    SharedPrefManager.getInstance(getActivity()).clear();
    Intent intent = new Intent(getActivity(),LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent);
  }
}
