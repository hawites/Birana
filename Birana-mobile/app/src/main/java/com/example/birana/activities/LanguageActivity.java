package com.example.birana.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.birana.R;
import com.example.birana.storage.SharedPrefManager;

import androidx.appcompat.app.AppCompatActivity;

public class LanguageActivity extends AppCompatActivity {

private Button confirm;
private RadioGroup radioButtonGroup;
private String lang;
private String language;

    @Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_language);
    confirm = findViewById(R.id.confirmb);
    radioButtonGroup = findViewById(R.id.radioGroup);


    confirm.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RadioButton rb = (RadioButton) findViewById( radioButtonGroup.getCheckedRadioButtonId());
            lang = rb.getText().toString();

            if(lang=="English"){
                language = "en";
            }
            else if(lang=="Amharic"){
                language="am";
            }

            SharedPrefManager.getInstance(LanguageActivity.this).addLanguage(language);

            startActivity(new Intent(LanguageActivity.this, GenreActivity.class));

        }
    });

}
}
