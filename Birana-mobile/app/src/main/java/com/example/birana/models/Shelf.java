package com.example.birana.models;



public class Shelf {
  private Double percent;
  private String start_date, finish_date;
  private int bookid;
  private String title;
  private String author_name;
  private String translator;
  private String description;
  private String genre;
  private Double price;
  private Double average_rating, total_ratings, peak_rating;
  private String language;
  private String published_date;
  private String app_lang;
  private String image, file;

  public Shelf(Double percent, String start_date, String finish_date, int bookid, String title, String author_name, String translator, String description, String genre, Double price, Double average_rating, Double total_ratings, Double peak_rating, String language, String published_date, String app_lang, String image, String file) {
    this.percent = percent;
    this.start_date = start_date;
    this.finish_date = finish_date;
    this.bookid = bookid;
    this.title = title;
    this.author_name = author_name;
    this.translator = translator;
    this.description = description;
    this.genre = genre;
    this.price = price;
    this.average_rating = average_rating;
    this.total_ratings = total_ratings;
    this.peak_rating = peak_rating;
    this.language = language;
    this.published_date = published_date;
    this.app_lang = app_lang;
    this.image = image;
    this.file = file;
  }

  public Double getPercent() {
    return percent;
  }

  public String getStart_date() {
    return start_date;
  }

  public String getFinish_date() {
    return finish_date;
  }

  public int getBookid() {
    return bookid;
  }

  public String getTitle() {
    return title;
  }

  public String getAuthor_name() {
    return author_name;
  }

  public String getTranslator() {
    return translator;
  }

  public String getDescription() {
    return description;
  }

  public String getGenre() {
    return genre;
  }

  public Double getPrice() {
    return price;
  }

  public Double getAverage_rating() {
    return average_rating;
  }

  public Double getTotal_ratings() {
    return total_ratings;
  }

  public Double getPeak_rating() {
    return peak_rating;
  }

  public String getLanguage() {
    return language;
  }

  public String getPublished_date() {
    return published_date;
  }

  public String getApp_lang() {
    return app_lang;
  }

  public String getImageurl() {
    return image;
  }

  public String getFileurl() {
    return file;
  }
}

