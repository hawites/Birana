
package com.example.birana.models;

import java.util.List;

public class ShelfResponse {
  private boolean error;
  private List<Shelf> shelf;

  public ShelfResponse(boolean error, List<Shelf> shelf) {
    this.error = error;
    this.shelf = shelf;
  }

  public boolean isError() {
    return error;
  }

  public List<Shelf> getShelf() {
    return shelf;
  }
}
