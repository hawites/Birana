package com.example.birana.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.birana.R;
import com.example.birana.activities.CurrentlyReadingActivity;
import com.example.birana.activities.LoginActivity;
import com.example.birana.activities.MainActivity;
import com.example.birana.activities.SingleBookViewActivity;
import com.example.birana.models.Book;
import com.example.birana.storage.SharedPrefManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class SAdapter extends RecyclerView.Adapter<SAdapter.SViewHolder> {
    private List<Book> books;
    private Context context;
    private static final String baseUrl = "http://192.168.43.148/";

    public SAdapter(List<Book> books, Context context) {
        this.books = books;
        this.context = context;
    }

    @NonNull
    @Override
    public SViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_search, parent, false);

        return new SViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SViewHolder holder, final int position) {
        Book book = books.get(position);
        holder.title.setText(book.getTitle());

        holder.author.setText(book.getAuthor_name());
        holder.averager.setText(book.getAverage_rating().toString());
        holder.totalr.setText(book.getTotal_ratings().toString());
        Picasso.get().load(baseUrl + book.getImage()).into(holder.img);
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("RecyclerView", "onClick：" + position);
                SharedPrefManager.getInstance(context).saveBook(books.get(position));
                v.getContext().startActivity(new Intent(context, SingleBookViewActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return books.size();
    }


    public static class SViewHolder extends RecyclerView.ViewHolder{
        TextView title, author, totalr, averager;
        ImageView img;
        LinearLayout linear;
        public SViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_r);
            author = itemView.findViewById(R.id.authorn);
            totalr = itemView.findViewById(R.id.trating);
            averager = itemView.findViewById(R.id.avrating);
            img = itemView.findViewById(R.id.bimg);
            linear = itemView.findViewById(R.id.linearsearch);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log.d("RecyclerView", "onClick：" + getAdapterPosition());
//                }
//            });
        }
    }
}
