
package com.example.birana.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;

import com.example.birana.R;
import com.example.birana.adapters.GenreAdapter;
import com.example.birana.api.RetrofitClient;
import com.example.birana.models.Genre;
import com.example.birana.models.GenreResponse;
import com.example.birana.storage.SharedPrefManager;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreActivity extends AppCompatActivity {
    private RecyclerView recyclerview;
    private GenreAdapter genreAdapter;
    private List<Genre> genre;
    private String lang;
    private ChipGroup chipGroup;
    private List<String> selectedGenre;
    private Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_genre);
        recyclerview = findViewById(R.id.recycler);
        lang = SharedPrefManager.getInstance(GenreActivity.this).getLanguage();
        submit = findViewById(R.id.submit);
        chipGroup = findViewById(R.id.filter_chip_group);
        selectedGenre = new ArrayList<String>();

        Call<GenreResponse> call = RetrofitClient.getInstance().getApi().getgenre(lang);
        call.enqueue(new Callback<GenreResponse>() {
            @Override
            public void onResponse(Call<GenreResponse> call, Response<GenreResponse> response) {
                genre = response.body().getGenre();
                // genreAdapter = new GenreAdapter(GenreActivity.this, genre);
                setTag(genre);
//                if(submit.isEnabled()){
//                    submit.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Log.i("hhhhh","");
//                            int i = 0;
//                            while(selectedGenre.size() > i){
//                                Log.i("val" + i, selectedGenre.get(i));
//                                i++;
//                            }
//                        }
//                    });
//                }

            }
            @Override
            public void onFailure(Call<GenreResponse> call, Throwable t) {

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GenreActivity.this, StoreFragment.class));
            }
        });
    }
    private void setTag(final List<Genre> tagList) {
//        final ChipGroup chipGroup = findViewById(R.id.filter_chip_group);
        for (int index = 0; index < tagList.size(); index++) {
            final String tagName = tagList.get(index).getGenre();
            final Chip chip = (Chip) getLayoutInflater().inflate(R.layout.recycler_genrelist, chipGroup,false);
            int paddingDp = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 10,
                    getResources().getDisplayMetrics()
            );
            chip.setPadding(2, paddingDp, paddingDp, paddingDp);
            chip.setText(tagName);
            chip.setTextColor(getResources().getColor(R.color.colorAccent));



            chip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        chip.setCheckedIconVisible(false);
                        chip.setChipBackgroundColor(getResources().getColorStateList(R.color.colorAccent));
                        chip.setTextColor(getResources().getColor(android.R.color.white));
                        selectedGenre.add(chip.getText().toString());

                    }
                    else{
                        chip.setCheckedIconVisible(false);
                        chip.setChipBackgroundColor(getResources().getColorStateList(android.R.color.white));
                        chip.setTextColor(getResources().getColor(R.color.colorAccent));
                        selectedGenre.remove(chip.getText().toString());
                    }

                    if (selectedGenre.size() >= 3){
                        submit.setText(R.string.genrenextbut);
                        submit.getBackground().setColorFilter(ContextCompat.getColor(GenreActivity.this, R.color.colorAccent), PorterDuff.Mode.MULTIPLY);
                        submit.setEnabled(true);

                    }
                    else{
                        submit.setText(R.string.genrebut);
                        submit.getBackground().setColorFilter(ContextCompat.getColor(GenreActivity.this, R.color.greyc), PorterDuff.Mode.MULTIPLY);
                        submit.setEnabled(false);

                    }


                }

            });
            //Added click listener on close icon to remove tag from ChipGroup

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });

            chipGroup.addView(chip);

        }
    }
}